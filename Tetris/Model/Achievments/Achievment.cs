﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Tetris.Model.Achievments
{
    public class Achievment
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public bool IsReached { get; set; }
        public BitmapImage Background { get; set; }
        public BitmapImage Icon { get; set; }

        public Func<bool> Predicate { get; set; }
    }
}
