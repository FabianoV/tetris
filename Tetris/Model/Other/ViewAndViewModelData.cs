﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tetris.Model.Other
{
    public class ViewAndViewModelData
    {
        public string Name { get; set; }
        public Type ViewType { get; set; }
        public Type ViewModelType { get; set; }
    }
}
