﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tetris.Model.Tetris.Serialization
{
    public class SerializableFigure
    {
        public List<SerializableBlock> Blocks { get; set; }
        public int Angle { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public string Type { get; set; }
    }
}
