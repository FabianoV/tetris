﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tetris.Model.Tetris.Serialization
{
    public class SerializableTetrisGameState
    {
        public SerializationMetadata Metadata { get; set; }
        public SerializableBoard Board { get; set; }
        public SerializableStatistics Statistics { get; set; }
    }
}
