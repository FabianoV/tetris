﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tetris.Model.Tetris.Serialization
{
    public class SerializationMetadata
    {
        public string SerializationName { get; set; }
        public DateTime SerializationTime { get; set; }
        public Version Version { get; set; }
    }
}
