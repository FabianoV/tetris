﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tetris.Model.Tetris.Serialization
{
    public class SerializableBoard
    {
        public SerializableFigure FallingFigure { get; set; }
        public SerializableFigure StoredFigure { get; set; }
        public List<SerializableFigure> NextFigures { get; set; }
        public List<SerializableBlock> Blocks { get; set; }

        public int Width { get; set; }
        public int Height { get; set; }
    }
}
