﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using Tetris.Model.Tetris.Serialization;

namespace Tetris.Model.Tetris
{
    public class SaveInfo
    {
        public string DirPath { get; set; }
        public string Name { get; set; }
        public DateTime Created { get; set; }
        public SerializableTetrisGameState GameState { get; set; }
        public BitmapImage ThumbImage { get; set; }
        public string ThumbFileName { get; set; }
        public string DataFileName { get; set; }
    }
}
