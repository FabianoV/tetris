﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tetris.Model.Tetris
{
    public class CurrentGameStatistics
    {
        public static CurrentGameStatistics Empty { get { return new CurrentGameStatistics(); } }

        public DateTime GameStart { get; set; }

        public int CleanedLines { get; set; }
        public int Points { get; set; }

        public int SingleLine { get; set; }
        public int DoubleLine { get; set; }
        public int TripleLine { get; set; }
        public int Tetris { get; set; }
    }
}
