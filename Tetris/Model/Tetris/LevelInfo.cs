﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tetris.Model.Tetris
{
    public class LevelInfo
    {
        public string Name { get; set; }
        public int FromCleanedLines { get; set; }
        public TimeSpan Speed { get; set; }
    }
}
