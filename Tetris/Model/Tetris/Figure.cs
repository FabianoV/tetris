﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace Tetris.Model
{
    public abstract class Figure
    {
        public List<Block> Blocks { get; set; }
        public int Angle { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public BitmapImage Texture { get; set; }
        public virtual string Type { get; set; }
        public virtual int NumberOfBlocks { get; set; }
        public List<Point> CurrentRotationData { get; set; }

        public virtual List<Point> Position0 { get; set; }
        public virtual List<Point> Position90 { get; set; }
        public virtual List<Point> Position180 { get; set; }
        public virtual List<Point> Position270 { get; set; }
    }
}
