﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Tetris.Model.Figures
{
    public class I : Figure
    {
        public override string Type { get; set; } = "blue";
        public override int NumberOfBlocks { get; set; } = 4;
        public override List<Point> Position0 { get; set; } = new List<Point>()
        {
            new Point(2, 0),
            new Point(2, 1),
            new Point(2, 2),
            new Point(2, 3),
        };
        public override List<Point> Position90 { get; set; } = new List<Point>()
        {
            new Point(0, 1),
            new Point(1, 1),
            new Point(2, 1),
            new Point(3, 1),
        };
        public override List<Point> Position180 { get; set; } = new List<Point>()
        {
            new Point(2, 0),
            new Point(2, 1),
            new Point(2, 2),
            new Point(2, 3),
        };
        public override List<Point> Position270 { get; set; } = new List<Point>()
        {
            new Point(0, 1),
            new Point(1, 1),
            new Point(2, 1),
            new Point(3, 1),
        };
    }
}
