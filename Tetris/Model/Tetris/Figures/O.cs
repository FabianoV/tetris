﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Tetris.Model.Figures
{
    public class O : Figure
    {
        public override string Type { get; set; } = "yellow";
        public override int NumberOfBlocks { get; set; } = 4;
        public override List<Point> Position0 { get; set; } = new List<Point>()
            {
                new Point(0, 0),
                new Point(0, 1),
                new Point(1, 0),
                new Point(1, 1),
            };
        public override List<Point> Position90 { get; set; } = new List<Point>()
            {
                new Point(0, 0),
                new Point(0, 1),
                new Point(1, 0),
                new Point(1, 1),
            };
        public override List<Point> Position180 { get; set; } = new List<Point>()
            {
                new Point(0, 0),
                new Point(0, 1),
                new Point(1, 0),
                new Point(1, 1),
            };
        public override List<Point> Position270 { get; set; } = new List<Point>()
        {
            new Point(0, 0),
            new Point(0, 1),
            new Point(1, 0),
            new Point(1, 1),
        };
    }
}
