﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Tetris.Model.Tetris.Textures
{
    public class TextureData
    {
        public string Name { get; set; }
        public string Path { get; set; }
        public BitmapImage Texture { get; set; }
    }
}
