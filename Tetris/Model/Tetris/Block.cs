﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Tetris.Model
{
    public class Block
    {
        public BitmapImage Texture { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
    }
}
