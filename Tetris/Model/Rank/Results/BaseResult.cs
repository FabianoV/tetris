﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace Tetris.Model.Rank.Results
{
    public class BaseResult
    {
        //Properties
        public bool HasError { get; set; }
        public string ErrorMessage { get; set; }
        public string ResponseMessage { get; set; }
        public HttpStatusCode StatusCode { get; set; }

        //Methods
        public virtual void Map(string responseMessage, HttpStatusCode statusCode, bool hasError=false, string errorMessage = null)
        {
            this.ResponseMessage = responseMessage;
            this.StatusCode = statusCode;
            this.ErrorMessage = errorMessage;
            this.HasError = hasError;
        }
    }
}
