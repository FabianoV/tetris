﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Tetris.Model.Rank.Results
{
    public class GetRankResult : BaseResult
    {
        public List<RankItem> RankItems { get; set; }

        public override void Map(string responseMessage, HttpStatusCode statusCode, bool hasError = false, string errorMessage = null)
        {
            base.Map(responseMessage, statusCode, hasError, errorMessage);

            this.RankItems = new List<RankItem>();

            if (!string.IsNullOrWhiteSpace(responseMessage))
            {
                this.RankItems = JsonConvert.DeserializeObject<List<RankItem>>(responseMessage);
            }
        }
    }
}
