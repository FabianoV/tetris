﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Tetris.Model.Rank.Results
{
    public class SetRankItemResult : BaseResult
    {
        public override void Map(string responseMessage, HttpStatusCode statusCode, bool hasError = false, string errorMessage = null)
        {
            base.Map(responseMessage, statusCode, hasError, errorMessage);

            if (string.IsNullOrWhiteSpace(responseMessage))
            {

            }
        }
    }
}
