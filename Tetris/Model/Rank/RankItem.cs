﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tetris.Model.Rank
{
    public class RankItem
    {
        public int Position { get; set; }
        public int Points { get; set; }
        public string Name { get; set; }
    }
}
