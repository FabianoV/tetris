using Microsoft.Practices.ServiceLocation;
using Tetris.ViewModel;
using System.Reflection;
using Autofac;
using Tetris.Services.Contract.Logic;

namespace Tetris
{
    public class AutoFacContainer
    {
        //Services
        public ContainerBuilder Builder { get; set; }
        public IContainer Container { get; set; }

        //Constructor
        public AutoFacContainer()
        {
            Assembly currentAssembly = Assembly.GetExecutingAssembly();
            this.Builder = new ContainerBuilder();
            this.Builder.RegisterAssemblyModules(currentAssembly);
            this.Container = this.Builder.Build();
            ServiceLocator.SetLocatorProvider(() => new AutoFacServiceLocator(this.Container));

            IGameComponents gameComponents = Container.Resolve<IGameComponents>();
        }

        //View Models
        public BackgroundViewModel Background { get { return Container.Resolve<BackgroundViewModel>(); } }
        public GameViewModel Game { get { return Container.Resolve<GameViewModel>(); } }
        public MainMenuViewModel MainMenu { get { return Container.Resolve<MainMenuViewModel>(); } }
        public GameSettingsViewModel Settings { get { return Container.Resolve<GameSettingsViewModel>(); } }
        public AchievmentsViewModel Achievments { get { return Container.Resolve<AchievmentsViewModel>(); } }
        public RankViewModel Rank { get { return Container.Resolve<RankViewModel>(); } }
        public AboutViewModel About { get { return Container.Resolve<AboutViewModel>(); } }
        public ControlsViewModel Controls { get { return Container.Resolve<ControlsViewModel>(); } }
        public GameModeViewModel GameMode { get { return Container.Resolve<GameModeViewModel>(); } }
        public SaveGameViewModel Save { get { return Container.Resolve<SaveGameViewModel>(); } }
        public LoadGameViewModel Load { get { return Container.Resolve<LoadGameViewModel>(); } }
        public MultiplayerRoomViewModel MultiplayerRoom { get { return Container.Resolve<MultiplayerRoomViewModel>(); } }
    }
}