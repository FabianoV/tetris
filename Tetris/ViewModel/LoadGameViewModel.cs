﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using Tetris.Model.Tetris;
using Tetris.Services.Contract.Other;
using Tetris.Services.Contract.Serialization;

namespace Tetris.ViewModel
{
    public class LoadGameViewModel : ViewModelBase, IOnNavigatedTo
    {
        //Services
        public INavigationService Navigation { get; set; }
        public IGameStateRepository GameStateRepository { get; set; }
        public ITetrisDeserializer Deserializer { get; set; }

        //Fields
        List<SaveInfo> savesList;
        SaveInfo selectedSave;

        //Proeprties
        public List<SaveInfo> SavesList
        {
            get { return savesList; }
            set { savesList = value; Refresh(); }
        }
        public SaveInfo SelectedSave
        {
            get { return selectedSave; }
            set { selectedSave = value; Refresh(); }
        }

        //Commands
        public RelayCommand LoadGameCommand { get; set; }
        public RelayCommand BackCommand { get; set; }
        public RelayCommand DeleteSaveCommand { get; set; }

        //Constructor
        public LoadGameViewModel(INavigationService navigation, IGameStateRepository gameStateRepository, ITetrisDeserializer deserializer)
        {
            //Services
            this.Navigation = navigation;
            this.GameStateRepository = gameStateRepository;
            this.Deserializer = deserializer;

            //Commands
            this.LoadGameCommand = new RelayCommand(LoadGame);
            this.BackCommand = new RelayCommand(Back);
            this.DeleteSaveCommand = new RelayCommand(DeleteSave);
        }

        //Methods
        public void OnNavigatedTo()
        {
            this.LoadSaveList();
        }

        public void LoadGame()
        {
            if (this.SelectedSave != null)
            {
                SaveInfo saveInfo = this.GameStateRepository.Load(this.SelectedSave);
                this.Deserializer.Deserialize(saveInfo.GameState);
            }
            this.Navigation.NavigateTo("GamePage");
        }

        public void DeleteSave()
        {
            this.GameStateRepository.Remove(this.SelectedSave);
            this.LoadSaveList();
        }

        public void LoadSaveList()
        {
            this.SavesList = this.GameStateRepository.GetSaveList().ToList();
            if (this.SavesList.Count() > 0)
            {
                this.SelectedSave = this.SavesList[0];
            }
            this.Refresh();
        }

        public void Back()
        {
            this.Navigation.GoBack();
        }

        private void Refresh()
        {
            this.RaisePropertyChanged(nameof(SavesList));
            this.RaisePropertyChanged(nameof(SelectedSave));
        }
    }
}
