﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using System;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Tetris.Model;
using Tetris.Model.Rank;
using Tetris.Services.Contract.Other;
using Tetris.Services.Contract.Rank;
using Tetris.Services.Contract.Drawing;
using Tetris.Services.Contract.Logic;
using Tetris.Services.Contract.Achievments;
using Tetris.Services.Contract.Serialization;

namespace Tetris.ViewModel
{
    public class GameViewModel : ViewModelBase, IOnNavigatedTo
    {
        //Services
        public IGameEngine GameEngine { get; set; }
        public IGameStatistics GameStatistics { get; set; }
        public ILevelManager LevelManager { get; set; }
        public IGameDrawingBitmap GameDrawing { get; set; }
        public INextFiveFiguresPanelDrawing NextFigureDrawing { get; set; }
        public IStoredFigurePanelDrawing StoredFigurePanelDrawing { get; set; }
        public IFigureMoving FigureMoving { get; set; }
        public IFigureRotation FigureRotation { get; set; }
        public IFigurePutter FigurePutter { get; set; }
        public IGameControl GameControl { get; set; }
        public INavigationService NavigationService { get; set; }
        public IRankApi RankApi { get; set; }
        public IAchievementsDataHolder AchievmentsHolder { get; set; }
        public IGameTimeCalculator GameTimeCalculator { get; set; }
        public ITetrisSerializer Serializer { get; set; }
        public ITetrisDeserializer Deserializer { get; set; }

        //Properties
        public BitmapImage Frame { get { return this.GameDrawing.Frame; } }
        public BitmapImage NextFigureFrame { get { return this.NextFigureDrawing.Frame; } }
        public BitmapImage StoredFigureFrame { get { return this.StoredFigurePanelDrawing.Frame; } }
        public string Points { get { return this.GameStatistics.Statistics.Points.ToString(); } }
        public string Level { get { return this.LevelManager.CurrentLevel.Name; } }
        public bool IsGamePaused { get { return this.GameEngine.IsPaused; } }
        public string PlayerName { get; set; }
        public IAchievment CurrentAchievment
        {
            get
            {
                if (this.AchievmentsHolder.AchievmentsLastReached != null && this.AchievmentsHolder.AchievmentsLastReached.Count > 0)
                {
                    return this.AchievmentsHolder.AchievmentsLastReached[0];
                }
                else
                {
                    return null;
                }
            }
        }
        public TimeSpan CurrentGameTime { get { return this.GameTimeCalculator.CurrentTime; } }

        //Visibility Properties
        public bool IsGameBoardVisible { get; set; }
        public bool IsStartButtonVisible { get; set; }
        public bool IsPauseMenuVisible { get; set; }
        public bool IsGameEndMenuVisible { get; set; }

        //Commands
        public RelayCommand StartGameCommand { get; set; }
        public RelayCommand ContinueGameCommand { get; set; }
        public RelayCommand ExitGameCommand { get; set; }
        public RelayCommand ShowEndGameMenuCommand { get; set; }
        public RelayCommand SaveScoreCommand { get; set; }
        public RelayCommand OpenControlsPanelCommand { get; set; }
        public RelayCommand CloseControlsPanelCommand { get; set; }
        public RelayCommand OpenControlsCommand { get; set; }
        public RelayCommand OpenSettingsCommand { get; set; }
        public RelayCommand SaveCommand { get; set; }
        public RelayCommand LoadCommand { get; set; }

        //Constructor
        public GameViewModel(IGameEngine gameEngine, IGameDrawingBitmap gameDrawing, IFigurePutter figurePutter,
            IGameStatistics gameStatistics, ILevelManager levelManager, INextFiveFiguresPanelDrawing nextFigureDrawing,
            IStoredFigurePanelDrawing storedFigureDrawing, IFigureMoving figureMoving, IFigureRotation figureRotation,
            IGameControl gameControl, INavigationService naviagtion, IRankApi rankApi, IAchievementsDataHolder achievmentsHolder,
            IGameTimeCalculator gameTimeCalculator, ITetrisSerializer serializer, ITetrisDeserializer deserializer)
        {
            //Services
            this.GameEngine = gameEngine;
            this.GameDrawing = gameDrawing;
            this.GameStatistics = gameStatistics;
            this.LevelManager = levelManager;
            this.NextFigureDrawing = nextFigureDrawing;
            this.StoredFigurePanelDrawing = storedFigureDrawing;
            this.FigurePutter = figurePutter;
            this.FigureMoving = figureMoving;
            this.FigureRotation = figureRotation;
            this.GameControl = gameControl;
            this.NavigationService = naviagtion;
            this.RankApi = rankApi;
            this.AchievmentsHolder = achievmentsHolder;
            this.GameTimeCalculator = gameTimeCalculator;
            this.Serializer = serializer;
            this.Deserializer = deserializer;

            //Commands
            this.StartGameCommand = new RelayCommand(this.StartGame);
            this.ContinueGameCommand = new RelayCommand(this.ContinueGame);
            this.ExitGameCommand = new RelayCommand(this.ExitGame);
            this.ShowEndGameMenuCommand = new RelayCommand(this.ShowEndGameMenu);
            this.SaveScoreCommand = new RelayCommand(this.SaveScore);
            this.OpenControlsCommand = new RelayCommand(this.OpenControls);
            this.OpenSettingsCommand = new RelayCommand(this.OpenSettings);
            this.SaveCommand = new RelayCommand(this.Save);
            this.LoadCommand = new RelayCommand(this.Load);

            //Initialization
            this.IsStartButtonVisible = true;
            this.IsGameBoardVisible = true;
            this.RedrawGame();

            //Game Events Reactions (for separate components)
            this.GameStatistics.PointCounterChanged += GameStatistics_PointCounterChanged;
            this.LevelManager.LevelChanged += LevelManager_LevelChanged;
            this.GameEngine.GamePaused += GameEngine_GamePaused;
            this.GameEngine.GameUnpaused += GameEngine_GameUnpaused;
            this.GameEngine.GameStarted += GameEngine_GameStarted; //Game Started
            this.GameEngine.GameEnded += GameEngine_GameEnded; ; //Game End
            this.GameEngine.GameMustRedraw += GameEngine_GameMustRedraw; //Game Redraw
            this.FigurePutter.FigurePuttedOnBoard += FigurePutter_FigurePuttedOnBoard; //Figure Putted
            this.FigureMoving.FigureMoveDown += FigureMoving_FigureMoveDown; //Figure Down
            this.FigureMoving.FigureMoveLeft += FigureMoving_FigureMoveLeft; //Figure Left
            this.FigureMoving.FigureMoveRight += FigureMoving_FigureMoveRight; //Figure Right
            this.FigureRotation.RotatedLeft += FigureMoving_FigureRotateLeft; //Figure Rotate Left
            this.FigureRotation.RotatedRight += FigureMoving_FigureRotateRight; //Figure Rotate Right
            this.AchievmentsHolder.AchivmentsReached += AchievmentsHolder_AchivmentsReached;
            this.GameTimeCalculator.NextSecond += GameTimeCalculator_NextSecond;
        }

        //Methods
        public void OnNavigatedTo()
        {
            this.RedrawGame();
        }

        public void Load()
        {
            this.NavigationService.NavigateTo("LoadGamePage");
            //this.Deserializer.Deserialize(GameState);
        }

        public void Save()
        {
            this.NavigationService.NavigateTo("SaveGamePage");
            //GameState = this.Serializer.Serialize();
        }

        public void StartGame()
        {
            this.GameEngine.StartGame();

            this.IsStartButtonVisible = false;
            this.RaisePropertyChanged(nameof(IsStartButtonVisible));

            this.IsGameEndMenuVisible = false;
            this.RaisePropertyChanged(nameof(IsGameEndMenuVisible));
        }

        public void ContinueGame()
        {
            this.GameEngine.Continue();
        }

        public void ExitGame()
        {
            this.GameEngine.Reset();
            this.IsStartButtonVisible = true;
            this.RaisePropertyChanged(nameof(IsStartButtonVisible));
            this.IsGameEndMenuVisible = false;
            this.RaisePropertyChanged(nameof(IsGameEndMenuVisible));
            this.NavigationService.NavigateTo("MainMenuPage");
        }

        public void ShowEndGameMenu()
        {
            this.GameEngine.EndGame();
            this.RaisePropertyChanged(nameof(IsGamePaused));
            this.IsPauseMenuVisible = false;
            this.RaisePropertyChanged(nameof(IsPauseMenuVisible));
            this.IsGameEndMenuVisible = true;
            this.RaisePropertyChanged(nameof(IsGameEndMenuVisible));
        }

        public void SaveScore()
        {
            if (!string.IsNullOrWhiteSpace(this.PlayerName))
            {
                RankItem rankItem = new RankItem()
                {
                    Name = this.PlayerName,
                    Points = this.GameStatistics.Statistics.Points,
                };

                this.RankApi.SendRankItem(rankItem);

                this.GameEngine.Reset();
                this.IsStartButtonVisible = true;
                this.RaisePropertyChanged(nameof(IsStartButtonVisible));
                this.IsGameEndMenuVisible = false;
                this.RaisePropertyChanged(nameof(IsGameEndMenuVisible));

                this.NavigationService.NavigateTo("RankPage");
            }
        }

        public void PauseGame()
        {
            this.GameEngine.Pause();
        }

        public void RedrawGame()
        {
            this.RedrawBoard();
            this.RedrawNextFigure();
            this.RedrawStoredFigure();
        }

        public void RedrawBoard()
        {
            this.GameDrawing.Redraw();
            this.RaisePropertyChanged(nameof(Frame));
        }

        public void RedrawNextFigure()
        {
            this.NextFigureDrawing.Redraw();
            this.RaisePropertyChanged(nameof(NextFigureFrame));
        }

        public void RedrawStoredFigure()
        {
            this.StoredFigurePanelDrawing.Redraw();
            this.RaisePropertyChanged(nameof(StoredFigureFrame));
        }

        public void SendKey(Key key)
        {
            this.GameControl.SendKey(key);
        }

        public void OpenControls()
        {
            this.NavigationService.NavigateTo("ControlsPage");
        }

        public void OpenSettings()
        {
            this.NavigationService.NavigateTo("SettingsPage");
        }

        #region Events
        //Events
        private void GameStatistics_PointCounterChanged(object sender, EventArgs e)
        {
            this.RaisePropertyChanged(nameof(Points));
        }
        private void LevelManager_LevelChanged(object sender, EventArgs e)
        {
            this.RaisePropertyChanged(nameof(Level));
        }
        private void GameEngine_GamePaused(object sender, EventArgs e)
        {
            this.RaisePropertyChanged(nameof(IsGamePaused));
        }
        private void GameEngine_GameUnpaused(object sender, EventArgs e)
        {
            this.RaisePropertyChanged(nameof(IsGamePaused));
        }
        private void FigurePutter_FigurePuttedOnBoard(object sender, Figure e)
        {
            this.RedrawGame();
        }
        private void GameEngine_GameMustRedraw(object sender, EventArgs e)
        {
            this.RedrawGame();
        }
        private void GameEngine_GameStarted(object sender, EventArgs e)
        {
            this.RedrawGame();
        }
        private void FigureMoving_FigureRotateLeft(object sender, Figure e)
        {
            this.RedrawGame();
        }
        private void FigureMoving_FigureRotateRight(object sender, Figure e)
        {
            this.RedrawGame();
        }
        private void FigureMoving_FigureMoveRight(object sender, Figure e)
        {
            this.RedrawGame();
        }
        private void FigureMoving_FigureMoveLeft(object sender, Figure e)
        {
            this.RedrawGame();
        }
        private void FigureMoving_FigureMoveDown(object sender, Figure e)
        {
            this.RedrawGame();
        }
        private void GameEngine_GameEnded(object sender, EventArgs e)
        {
            this.IsGameEndMenuVisible = true;
            this.RaisePropertyChanged(nameof(IsGameEndMenuVisible));
        }
        private void AchievmentsHolder_AchivmentsReached(object sender, EventArgs e)
        {
            this.CurrentAchievment.FakeIsReached(false);
            this.RaisePropertyChanged(nameof(CurrentAchievment));
            this.CurrentAchievment.DisableFake();
            this.RaisePropertyChanged(nameof(CurrentAchievment));
        }
        private void GameTimeCalculator_NextSecond(object sender, EventArgs e)
        {
            this.RaisePropertyChanged(nameof(CurrentGameTime));
        }
        #endregion
    }
}
