﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tetris.ViewModel
{
    public class ControlsViewModel : ViewModelBase
    {
        //Services
        public INavigationService Navigation { get; set; }

        //Proeprties
        public RelayCommand BackCommand { get; set; }

        //Constructor
        public ControlsViewModel(INavigationService navigation)
        {
            //Services
            this.Navigation = navigation;

            //Commands
            this.BackCommand = new RelayCommand(Back);
        }

        //Methods
        public void Back()
        {
            this.Navigation.GoBack();
        }
    }
}
