﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using Tetris.Services.Contract.Other;

namespace Tetris.ViewModel
{
    public class BackgroundViewModel : ViewModelBase
    {
        //Services

        //Properties
        public bool IsBackgroundVisible { get; set; }
        public string ImageSource { get; set; }
        
        //Constructor
        public BackgroundViewModel()
        {
            //Services

            //Initialize
            this.ImageSource = "/Tetris;component/Content/background/background5_16do9.gif";
            this.IsBackgroundVisible = true;
        }

        //Methods
        public void SetVisible()
        {
            this.IsBackgroundVisible = true;
            this.RaisePropertyChanged(nameof(IsBackgroundVisible));
        }

        public void SetCollapsed()
        {
            this.IsBackgroundVisible = false;
            this.RaisePropertyChanged(nameof(IsBackgroundVisible));
        }
    }
}
