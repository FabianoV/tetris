﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using System;
using System.Collections.Generic;
using Tetris.Model.Rank;
using Tetris.Model.Rank.Results;
using Tetris.Services.Contract.Other;
using Tetris.Services.Contract.Rank;

namespace Tetris.ViewModel
{
    public class RankViewModel : ViewModelBase, IOnNavigatedTo
    {
        //Services
        public INavigationService Navigation { get; set; }
        public IRankHolder RankHolder { get; set; }
        public IRankApi RankApi { get; set; }

        //Properties
        public bool IsRankVisible { get; set; }
        public List<RankItem> RankItems { get; set; }

        //Commands
        public RelayCommand BackCommand { get; set; }

        //Constructor
        public RankViewModel(INavigationService navigation, IRankHolder rankHolder, IRankApi rankApi)
        {
            //Services
            this.Navigation = navigation;
            this.RankHolder = rankHolder;
            this.RankApi = rankApi;

            //Commands
            this.BackCommand = new RelayCommand(BackToMainMenu);

            //Initialize
            this.IsRankVisible = true;
            this.RankItems = GetRankItems();
        }

        //Methods
        public void OnNavigatedTo()
        {
            this.RankItems = GetRankItems();
        }

        public void BackToMainMenu()
        {
            this.Navigation.NavigateTo("MainMenuPage");
        }

        public void SetVisible()
        {
            this.IsRankVisible = true;
            this.RaisePropertyChanged(nameof(IsRankVisible));
        }

        public void SetCollapsed()
        {
            this.IsRankVisible = false;
            this.RaisePropertyChanged(nameof(IsRankVisible));
        }

        private List<RankItem> GetRankItems()
        {
            GetRankResult requestResult = this.RankApi.GetRank(limit: 10, offset: 0);
            List<RankItem> result = null;

            if (requestResult.HasError)
            {
                result = new List<RankItem>();
            }
            else
            {
                result = requestResult.RankItems;
            }

            return result;
        }
    }
}
