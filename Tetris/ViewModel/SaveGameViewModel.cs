﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using Tetris.Model.Tetris;
using Tetris.Services.Contract.Other;
using Tetris.Services.Contract.Drawing;
using Tetris.Services.Contract.Serialization;

namespace Tetris.ViewModel
{
    public class SaveGameViewModel : ViewModelBase, IOnNavigatedTo
    {
        //Services
        public INavigationService Navigation { get; set; }
        public ITetrisSerializer Serializer { get; set; }
        public IGameStateRepository GameStateRepository { get; set; }
        public IGameDrawingBitmap BitmapDrawingService { get; set; }

        //Fields
        List<SaveInfo> savesList;
        SaveInfo selectedSave;

        //Proeprties
        public List<SaveInfo> SavesList
        {
            get { return savesList; }
            set { savesList = value; Refresh(); }
        }
        public SaveInfo SelectedSave
        {
            get { return selectedSave; }
            set { selectedSave = value; Refresh(); }
        }

        //Commands
        public RelayCommand BackCommand { get; set; }
        public RelayCommand SaveGameCommand { get; set; }
        public RelayCommand DeleteSaveCommand { get; set; }

        //Constructor
        public SaveGameViewModel(INavigationService navigation, ITetrisSerializer serializer, IGameStateRepository gameStateRepository,
            IGameDrawingBitmap bitmapDrawingService)
        {
            //Services
            this.Navigation = navigation;
            this.Serializer = serializer;
            this.GameStateRepository = gameStateRepository;
            this.BitmapDrawingService = bitmapDrawingService;

            //Commands
            this.BackCommand = new RelayCommand(Back);
            this.SaveGameCommand = new RelayCommand(SaveGame);
            this.DeleteSaveCommand = new RelayCommand(DelteGame);
        }

        //Methods
        public void OnNavigatedTo()
        {
            this.LoadSaveList();
        }

        public void SaveGame()
        {
            SaveInfo info = new SaveInfo()
            {
                ThumbImage = this.BitmapDrawingService.Frame,
            };
            info = this.GameStateRepository.Save(info);
            this.LoadSaveList();
        }

        public void DelteGame()
        {
            this.GameStateRepository.Remove(this.SelectedSave);
            this.LoadSaveList();
        }

        public void LoadSaveList()
        {
            this.SavesList = this.GameStateRepository.GetSaveList().ToList();
            if (this.SavesList.Count() > 0)
            {
                this.SelectedSave = this.SavesList[0];
            }
            this.Refresh();
        }

        public void Back()
        {
            this.Navigation.GoBack();
        }

        private void Refresh()
        {
            this.RaisePropertyChanged(nameof(SavesList));
            this.RaisePropertyChanged(nameof(SelectedSave));
        }
    }
}
