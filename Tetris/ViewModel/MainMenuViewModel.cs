﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using Tetris.Services.Contract.Logic;
using Tetris.Services.Contract.Other;

namespace Tetris.ViewModel
{
    public class MainMenuViewModel : ViewModelBase
    {
        //Services
        public IGameEngine GameEngine { get; set; }
        public INavigationService Navigation { get; set; }
        public IApplicationSpecialActions Application { get; set; }

        //Properties
        public bool IsMainMenuVisible { get; set; }

        //Commands
        public RelayCommand StartGameCommand { get; set; }
        public RelayCommand OpenRankCommand { get; set; }
        public RelayCommand OpenSettingsCommand { get; set; }
        public RelayCommand OpenAchievmentsCommand { get; set; }
        public RelayCommand ExitGameCommand { get; set; }
        public RelayCommand OpenFacebookPageCommand { get; set; }
        public RelayCommand OpenInternetPageCommand { get; set; }
        public RelayCommand OpenAboutCommand { get; set; }
        public RelayCommand OpenControlsPageCommand { get; set; }

        //Constructor
        public MainMenuViewModel(IGameEngine gameEngine, INavigationService navigation, IApplicationSpecialActions application)
        {
            //Services
            this.GameEngine = gameEngine;
            this.Navigation = navigation;
            this.Application = application;

            //Commands
            this.StartGameCommand = new RelayCommand(StartGame);
            this.OpenRankCommand = new RelayCommand(OpenRank);
            this.OpenSettingsCommand = new RelayCommand(OpenSettings);
            this.OpenAchievmentsCommand = new RelayCommand(OpenAchievments);
            this.ExitGameCommand = new RelayCommand(ExitGame);
            this.OpenFacebookPageCommand = new RelayCommand(OpenFacebookPage);
            this.OpenInternetPageCommand = new RelayCommand(OpenInternetPage);
            this.OpenAboutCommand = new RelayCommand(OpenAbout);
            this.OpenControlsPageCommand = new RelayCommand(OpenControlsPage);

            //Initialize
            this.IsMainMenuVisible = true;
        }

        //Methods
        public void StartGame()
        {
            this.Navigation.NavigateTo("GameModePage");
        }

        public void OpenRank()
        {
            this.Navigation.NavigateTo("RankPage");
        }

        public void OpenSettings()
        {
            this.Navigation.NavigateTo("SettingsPage");
        }

        public void OpenAchievments()
        {
            this.Navigation.NavigateTo("AchievmentsPage");
        }

        public void OpenAbout()
        {
            this.Navigation.NavigateTo("AboutPage");
        }

        public void OpenControlsPage()
        {
            this.Navigation.NavigateTo("ControlsPage");
        }

        public void ExitGame()
        {
            this.Application.ExitApplication();
        }

        public void OpenFacebookPage()
        {
            Process.Start("https://www.facebook.com/Tetris-110800092944992/");
        }

        public void OpenInternetPage()
        {
            Process.Start("http://www.fol.cba.pl/cv");
        }
    }
}
