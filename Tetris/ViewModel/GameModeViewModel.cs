﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tetris.ViewModel
{
    public class GameModeViewModel : ViewModelBase
    {
        //Services
        public INavigationService Navigation { get; set; }

        //Properties
        public RelayCommand RunNormalGameCommand { get; set; }
        public RelayCommand RunMultiplayerGameCommand { get; set; }
        public RelayCommand OpenLoadGamePageCommand { get; set; }
        public RelayCommand BackCommand { get; set; }

        //Constructor
        public GameModeViewModel(INavigationService navigation)
        {
            //Services
            this.Navigation = navigation;

            //Commands
            this.RunNormalGameCommand = new RelayCommand(RunNormalGame);
            this.OpenLoadGamePageCommand = new RelayCommand(OpenLoadGamePage);
            this.RunMultiplayerGameCommand = new RelayCommand(RunMultiplayerGame);
            this.BackCommand = new RelayCommand(Back);
        }

        //Methods
        public void RunNormalGame()
        {
            this.Navigation.NavigateTo("GamePage");
        }

        public void RunMultiplayerGame()
        {
            this.Navigation.NavigateTo("MultiplayerRoomPage");
        }

        public void OpenLoadGamePage()
        {
            this.Navigation.NavigateTo("LoadGamePage");
        }

        public void Back()
        {
            this.Navigation.NavigateTo("MainMenuPage");
        }
    }
}
