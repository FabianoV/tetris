﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Model.Tetris;
using Tetris.Services.Contract.Rank;
using Tetris.Services.Contract.Achievments;
using Tetris.Services.Contract.Logic;

namespace Tetris.ViewModel
{
    public class GameSettingsViewModel : ViewModelBase
    {
        //Services
        public IGameEngine GameEngine { get; set; }
        public INavigationService Navigation { get; set; }
        public IRankApi RankApi { get; set; }
        public IAchievmentsDataCache AchievmentsCache { get; set; }
        public IAchievementsDataHolder AchievmentsHolder { get; set; }

        //Properties
        public bool IsGameSettingsVisible { get; set; }
        public bool IsWindowMaximized { get; set; }
        public bool IsWindowBarVisible { get; set; }

        //Commands
        public RelayCommand BackCommand { get; set; }
        public RelayCommand ChangeWindowStateCommand { get; set; }
        public RelayCommand ResetRankCommand { get; set; }
        public RelayCommand ResetStatsCommand { get; set; }

        //Constructor
        public GameSettingsViewModel(IGameEngine gameEngineService, INavigationService navigation, IRankApi rankApi, 
            IAchievmentsDataCache achievmentsCache, IAchievementsDataHolder achievmentsHolder)
        {
            //Services
            this.GameEngine = gameEngineService;
            this.Navigation = navigation;
            this.RankApi = rankApi;
            this.AchievmentsCache = achievmentsCache;
            this.AchievmentsHolder = achievmentsHolder;

            //Commands
            this.BackCommand = new RelayCommand(this.Back);
            this.ChangeWindowStateCommand = new RelayCommand(this.ChangeWindowState);
            this.ResetRankCommand = new RelayCommand(this.ResetRank);
            this.ResetStatsCommand = new RelayCommand(this.ResetStats);

            //Initialize
            this.IsGameSettingsVisible = true;
            this.IsWindowMaximized = false;
            this.IsWindowBarVisible = true;
        }

        private void ResetStats()
        {
            this.AchievmentsCache.ResetAchievmentsData();
            this.AchievmentsHolder.CheckAchievmentsNowReached(CurrentGameStatistics.Empty);
        }

        //Methods
        public void ChangeWindowState()
        {
            this.IsWindowMaximized = !this.IsWindowMaximized;
            this.IsWindowBarVisible = !this.IsWindowBarVisible;
            this.RaisePropertyChanged(nameof(IsWindowBarVisible));
            this.RaisePropertyChanged(nameof(IsWindowMaximized));
        }

        public void Back()
        {
            this.Navigation.GoBack();
        }

        public void ResetRank()
        {
            this.RankApi.ResetRank();
        }
    }
}
