﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tetris.ViewModel
{
    public class MultiplayerRoomViewModel : ViewModelBase
    {
        //Properties
        public INavigationService Navigation { get; set; }

        //Commands
        public RelayCommand BackCommand { get; set; }
        public RelayCommand RefreshCommand { get; set; }
        public RelayCommand JoinRoomCommand { get; set; }
        public RelayCommand CreateRoomCommand { get; set; }

        //Constructor
        public MultiplayerRoomViewModel(INavigationService navigation)
        {
            //Services
            this.Navigation = navigation;

            //Commands
            this.BackCommand = new RelayCommand(Back);
            this.RefreshCommand = new RelayCommand(Refresh);
            this.JoinRoomCommand = new RelayCommand(JoinRoom);
            this.CreateRoomCommand = new RelayCommand(CreateRoom);
        }

        //Methods
        public void JoinRoom()
        {

        }

        public void CreateRoom()
        {

        }

        public void Back()
        {
            this.Navigation.GoBack();
        }

        private void Refresh()
        {
            //this.RaisePropertyChanged(nameof(SavesList));
            //this.RaisePropertyChanged(nameof(SelectedSave));
        }
    }
}
