﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using Tetris.Model.Achievments;
using Tetris.Services.Contract.Achievments;

namespace Tetris.ViewModel
{
    public class AchievmentsViewModel : ViewModelBase
    {
        //Services
        public INavigationService Navigation { get; set; }
        public IAchievementsDataHolder AchievmentsHolder { get; set; }

        //Properties
        public List<IAchievment> AchievementsList { get { return AchievmentsHolder.Achievments; } }

        //Commands
        public RelayCommand BackCommand { get; set; }

        //Constructor
        public AchievmentsViewModel(INavigationService navigation, IAchievementsDataHolder achievmentsHolder)
        {
            //Services
            this.Navigation = navigation;
            this.AchievmentsHolder = achievmentsHolder;

            //Commands
            this.BackCommand = new RelayCommand(this.Back);
        }

        //Methods
        public void Back()
        {
            this.Navigation.NavigateTo("MainMenuPage");
        }
    }
}
