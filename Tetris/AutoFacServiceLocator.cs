﻿using Autofac;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tetris
{
    public class AutoFacServiceLocator : IServiceLocator
    {
        //Properties
        public IContainer AutoFacContainer { get; set; }

        //Constructor
        public AutoFacServiceLocator(IContainer container)
        {
            this.AutoFacContainer = container;
        }

        //Methods
        public IEnumerable<object> GetAllInstances(Type serviceType)
        {
            var method = typeof(AutoFacServiceLocator).GetMethod("GetAllInstances");
            var genericMethod = method.MakeGenericMethod(serviceType);
            return genericMethod.Invoke(this, null) as IEnumerable<object>;
        }

        public IEnumerable<TService> GetAllInstances<TService>()
        {
            return this.AutoFacContainer.Resolve<IEnumerable<TService>>();
        }

        public object GetInstance(Type serviceType)
        {
            return this.AutoFacContainer.Resolve(serviceType);
        }

        public object GetInstance(Type serviceType, string key)
        {
            return this.AutoFacContainer.ResolveNamed(key, serviceType);
        }

        public TService GetInstance<TService>()
        {
            return this.AutoFacContainer.Resolve<TService>();
        }

        public TService GetInstance<TService>(string key)
        {
            return this.AutoFacContainer.ResolveNamed<TService>(key);
        }

        public object GetService(Type serviceType)
        {
            return this.AutoFacContainer.Resolve(serviceType);
        }
    }
}
