﻿using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Tetris.Model;
using Tetris.Services.Contract;
using Tetris.Services.Contract.Other;
using Tetris.Services.Contract.Drawing;
using Tetris.Services.Contract.Logic;
using Tetris.ViewModel;
using Microsoft.Practices.ServiceLocation;

namespace Tetris
{
    public partial class MainWindow : Window
    {
        //View Models
        public GameViewModel Game { get; set; }

        //Services
        public INavigationInitializer NavigationInitializer { get; set; }
        public INavigationService Navigation { get; set; }

        public MainWindow()
        {
            InitializeComponent();

            //View Models
            this.Game = ServiceLocator.Current.GetInstance<GameViewModel>();

            //Sevices
            this.NavigationInitializer = ServiceLocator.Current.GetInstance<INavigationInitializer>();
            this.Navigation = ServiceLocator.Current.GetInstance<INavigationService>();

            //Initialize
            this.NavigationInitializer.Initialize(this.ScreenFrame);
            this.Navigation.NavigateTo("MainMenuPage");
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            this.Game.SendKey(e.Key);
        }
    }
}
