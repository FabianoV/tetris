﻿using System;
using System.Linq;
using System.Reflection;
using Autofac;

namespace Tetris.Modules
{
    public class AchievmentsModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            string @namespace = "Tetris.Services.Implementation.Achievments";
            Type[] achivmentsServicesTypes = assembly.GetTypes().Where(type => type.Namespace == @namespace).ToArray();
            builder.RegisterTypes(achivmentsServicesTypes).AsImplementedInterfaces().SingleInstance();
        }
    }
}
