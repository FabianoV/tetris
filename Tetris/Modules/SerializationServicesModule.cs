﻿using System;
using System.Linq;
using System.Reflection;
using Autofac;

namespace Tetris.Modules
{
    public class SerializationServicesModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            string @namespace = "Tetris.Services.Implementation.Serialization";
            Type[] serializationTypes = assembly.GetTypes().Where(type => type.Namespace == @namespace).ToArray();
            builder.RegisterTypes(serializationTypes).AsImplementedInterfaces().SingleInstance();
        }
    }
}
