﻿using System;
using System.Linq;
using System.Reflection;
using Autofac;

namespace Tetris.Modules
{
    public class LogicServicesModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            string @namespace = "Tetris.Services.Implementation.Logic";
            Type[] logicServicesTypes = assembly.GetTypes().Where(type => type.Namespace == @namespace).ToArray();
            builder.RegisterTypes(logicServicesTypes).AsImplementedInterfaces().SingleInstance();
        }
    }
}
