﻿using Autofac;
using GalaSoft.MvvmLight;
using System;
using System.Linq;
using System.Reflection;

namespace Tetris.Modules
{
    public class ViewModelsModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            Type[] viewModelsTypes = assembly.GetTypes().Where(type => type.IsSubclassOf(typeof(ViewModelBase))).ToArray();
            builder.RegisterTypes(viewModelsTypes).AsSelf().SingleInstance();
        }
    }
}
