﻿using System;
using System.Linq;
using System.Reflection;
using Autofac;

namespace Tetris.Modules
{
    public class RankServicesModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            string @namespace = "Tetris.Services.Implementation.Rank";
            Type[] rankServicesTypes = assembly.GetTypes().Where(type => type.Namespace == @namespace).ToArray();
            builder.RegisterTypes(rankServicesTypes).AsImplementedInterfaces().SingleInstance();
        }
    }
}
