﻿using Autofac;
using System;
using System.Linq;
using System.Reflection;

namespace Tetris.Modules
{
    public class DrawingServicesModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            string @namespace = "Tetris.Services.Implementation.Drawing";
            Type[] drawingServicesTypes = assembly.GetTypes().Where(type => type.Namespace == @namespace).ToArray();
            builder.RegisterTypes(drawingServicesTypes).AsImplementedInterfaces().SingleInstance();
        }
    }
}
