﻿using System;
using System.Linq;
using System.Reflection;
using Autofac;

namespace Tetris.Modules
{
    public class MultiplayerServicesModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            string @namespace = "Tetris.Services.Implementation.Multiplayer";
            Type[] multiplayerServicesTypes = assembly.GetTypes().Where(type => type.Namespace == @namespace).ToArray();
            builder.RegisterTypes(multiplayerServicesTypes).AsImplementedInterfaces().SingleInstance();
        }
    }
}
