﻿using System;
using System.Linq;
using System.Reflection;
using Autofac;
using Tetris.Services.Implementation.Other;

namespace Tetris.Modules
{
    public class OtherServicesModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            string @namespace = "Tetris.Services.Implementation.Other";
            Type[] otherTypes = assembly.GetTypes().Where(type => type.Namespace == @namespace).ToArray();
            builder.RegisterTypes(otherTypes).AsImplementedInterfaces().SingleInstance();
        }
    }
}
