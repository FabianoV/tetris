﻿using System;
using System.Linq;
using System.Reflection;
using Autofac;
using Tetris.Services.Contract.Achievments;
using Tetris.Services.Implementation.Achievments.Items;

namespace Tetris.Modules
{
    public class AchievmentItemsModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            Type[] achivmentsItemsTypes = assembly.GetTypes().Where(type => type.IsSubclassOf(typeof(BaseAchievment))).ToArray();
            builder.RegisterTypes(achivmentsItemsTypes).AsSelf().As<BaseAchievment>().AsImplementedInterfaces().SingleInstance();
        }
    }
}
