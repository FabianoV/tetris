﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Tetris.Converters
{
    public class BooleanToWindowStyleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool val = System.Convert.ToBoolean(value);

            if (val == true)
            {
                return WindowStyle.ToolWindow;
            }
            else
            {
                return WindowStyle.None;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            WindowStyle ws = (WindowStyle)value;

            switch (ws)
            {
                case WindowStyle.None: return false;
                case WindowStyle.SingleBorderWindow:
                case WindowStyle.ThreeDBorderWindow:
                case WindowStyle.ToolWindow: return true;
                default: throw new InvalidCastException();
            }

            //return false;
        }
    }
}
