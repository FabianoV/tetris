﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Tetris.Converters
{
    public class DateTimeToStringFormattedConverter : IValueConverter
    {
        string format = "dd/MM/yyyy H:mm:ss";

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return System.Convert.ToDateTime(value).ToString(format);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return DateTime.ParseExact(value as string, format, CultureInfo.InvariantCulture);
        }
    }
}
