﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Tetris.Converters
{
    public class BooleanToMaximizedWindowStateConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool val = System.Convert.ToBoolean(value);

            if (val == true)
            {
                return WindowState.Maximized;
            }
            else
            {
                return WindowState.Normal;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            WindowState ws = (WindowState)value;

            switch (ws)
            {
                case WindowState.Normal:
                case WindowState.Minimized: return false;
                case WindowState.Maximized: return true;
                default: throw new InvalidCastException();
            }

            //return false;
        }
    }
}
