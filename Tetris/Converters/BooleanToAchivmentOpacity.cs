﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Tetris.Converters
{
    class BooleanToAchivmentOpacity : IValueConverter
    {
        public BooleanToAchivmentOpacity()
        {

        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool val = System.Convert.ToBoolean(value);

            if (val == true)
            {
                return 1;
            }
            else
            {
                return 0.5;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double val = System.Convert.ToDouble(value);

            if (val == 0.5)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
