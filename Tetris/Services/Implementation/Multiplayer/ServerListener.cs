﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Tetris.Services.Implementation.Multiplayer
{
    public class ServerListener
    {
        //Properties
        public IPAddress LocalAddress { get; set; } = IPAddress.Parse("127.0.0.1");
        public int Port { get; set; } = 13000;
        public TcpListener Server { get; set; }

        public byte[] Bytes { get; set; } = new byte[256];

        //Methods
        public void Listen()
        {
            this.Server = new TcpListener(LocalAddress, Port);
            this.Server.Start();

            try
            {
                this.TryListen();
            }
            catch (SocketException e)
            {
                Console.WriteLine("SocketException: {0}", e);
                //TODO: Logger
            }
            finally
            {
                Server.Stop();
            }
        }

        public void TryListen()
        {
            while (true)
            {
                TcpClient client = Server.AcceptTcpClient();
                NetworkStream stream = client.GetStream();
                int bytesCount;
                while ((bytesCount = stream.Read(Bytes, 0, Bytes.Length)) != 0)
                {
                    string receivedData = Encoding.ASCII.GetString(Bytes, 0, bytesCount);
                    Console.WriteLine("Received: {0}", receivedData);
                    string processedData = ProcessData(receivedData);
                    byte[] message = Encoding.ASCII.GetBytes(processedData);
                    stream.Write(message, 0, message.Length); // Send back a response.
                    Console.WriteLine("Sent: {0}", processedData);
                }

                client.Close();
            }
        }

        private string ProcessData(string data)
        {
            string response = "";

            response = data.ToUpper();

            return response;
        }
    }
}
