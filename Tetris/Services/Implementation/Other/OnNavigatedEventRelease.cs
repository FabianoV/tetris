﻿using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Model.Other;
using Tetris.Services.Contract.Other;
using Tetris.View;
using Tetris.ViewModel;

namespace Tetris.Services.Implementation.Other
{
    public class OnNavigatedEventRelease : IOnNavigatedEventRelease
    {
        //Properties
        public ViewAndViewModelDataCollection Data { get; set; }

        //Constructor
        public OnNavigatedEventRelease()
        {
            this.Data = new ViewAndViewModelDataCollection();
            this.Data.Register("GamePage", typeof(GamePage), typeof(GameViewModel));
            this.Data.Register("MainMenuPage", typeof(MainMenuPage), typeof(MainMenuViewModel));
            this.Data.Register("RankPage", typeof(RankPage), typeof(RankViewModel));
            this.Data.Register("SettingsPage", typeof(SettingsPage), typeof(GameSettingsViewModel));
            this.Data.Register("SaveGamePage", typeof(SaveGamePage), typeof(SaveGameViewModel));
            this.Data.Register("LoadGamePage", typeof(LoadGamePage), typeof(LoadGameViewModel));
        }

        //Methods
        public void Release(string viewName)
        {
            Type viewModelType = this.Data.FindViewModelTypeByName(viewName);
            if (viewModelType != null)
            {
                object instance = ServiceLocator.Current.GetInstance(viewModelType);
                if (instance is IOnNavigatedTo)
                {
                    IOnNavigatedTo instanceChecked = instance as IOnNavigatedTo;
                    instanceChecked.OnNavigatedTo();
                }
            }
        }
    }

    public class ViewAndViewModelDataCollection
    {
        public List<ViewAndViewModelData> Data { get; set; } = new List<ViewAndViewModelData>();

        public void Register(string name, Type view, Type viewModel)
        {
            ViewAndViewModelData vavmd = new ViewAndViewModelData()
            {
                Name = name,
                ViewType = view,
                ViewModelType = viewModel,
            };
            this.Data.Add(vavmd);
        }
        public Type FindViewTypeByName(string name)
        {
            ViewAndViewModelData data = this.Data.FirstOrDefault(d => d.Name == name);

            if (data != null)
            {
                return data.ViewType;
            }

            return null;
        }
        public Type FindViewModelTypeByName(string name)
        {
            ViewAndViewModelData data = this.Data.FirstOrDefault(d => d.Name == name);

            if (data != null)
            {
                return data.ViewModelType;
            }

            return null;
        }
    }
}
