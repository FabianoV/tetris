﻿using GalaSoft.MvvmLight.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Tetris.Services.Contract.Other;

namespace Tetris.Services.Implementation.Other
{
    public class NavigationService : INavigationService, INavigationInitializer
    {
        //Services
        public IOnNavigatedEventRelease NavigatedToRelease { get; set; }

        //Properties
        private Frame Frame { get; set; }
        public string CurrentPageKey { get; set; }
        public Dictionary<string, string> PagesKeys { get; set; }
        public List<string> History { get; set; }

        //Constructor
        public NavigationService(IOnNavigatedEventRelease naviagetdToEventRelease)
        {
            //Services
            this.NavigatedToRelease = naviagetdToEventRelease;

            //Initialize
            this.PagesKeys = new Dictionary<string, string>() { };
            this.History = new List<string>();
        }

        //Methods
        public void Initialize(Frame frame)
        {
            this.Frame = frame;
        }

        public void GoBack()
        {
            string nextPage = this.History[this.History.Count - 2];
            this.History.Add(nextPage);
            this.Frame.Source = new Uri(nextPage, UriKind.Relative);
        }

        public void NavigateTo(string pageKey)
        {
            this.NavigateTo(pageKey, null);
        }

        public void NavigateTo(string pageKey, object parameter)
        {
            string nextPage = "MainMenuPage.xaml";

            if (this.PagesKeys.ContainsKey(pageKey))
            {
                nextPage = this.PagesKeys[pageKey];
            }
            else
            {
                nextPage = pageKey + ".xaml";
            }

            this.History.Add(nextPage);
            this.Frame.Source = new Uri(nextPage, UriKind.Relative);
            this.NavigatedToRelease.Release(pageKey);
        }
    }
}
