﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using Tetris.Services.Contract.Other;

namespace Tetris.Services.Implementation.Other
{
    public class GameTimeCalculator : IGameTimeCalculator
    {
        //Calculate time in game Timer

        //Properties
        public DispatcherTimer Timer { get; set; }
        public DateTime StartTime { get; set; }
        public TimeSpan CurrentTime { get; set; }
        public TimeSpan TimeSum { get; set; }

        //Events
        public event EventHandler NextSecond;

        //Constructor
        public GameTimeCalculator()
        {
            //Initialize
            this.Timer = new DispatcherTimer();
            this.Timer.Interval = TimeSpan.FromMilliseconds(500);
            this.Timer.Tick += Timer_Tick;
        }

        //Methods
        public void Start()
        {
            this.StartTime = DateTime.Now;
            this.Timer.Start();
        }

        public void Stop()
        {
            this.Timer.Stop();
            this.TimeSum = TimeSum + (DateTime.Now - this.StartTime);
        }

        public void Reset()
        {
            this.TimeSum = TimeSpan.Zero;
            this.CurrentTime = TimeSpan.Zero;
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            this.CurrentTime = this.TimeSum + (DateTime.Now - this.StartTime);
            this.NextSecond?.Invoke(this, EventArgs.Empty);
        }
    }
}
