﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Tetris.Services.Contract.Other;

namespace Tetris.Services.Implementation.Other
{
    public class ApplicationActions : IApplicationSpecialActions
    {
        public void ExitApplication()
        {
            Application.Current.Shutdown();
        }
    }
}
