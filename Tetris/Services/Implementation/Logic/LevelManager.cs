﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Model.Tetris;
using Tetris.Services.Contract.Logic;

namespace Tetris.Services.Implementation.Logic
{
    public class LevelManager : ILevelManager
    {
        //Services
        public IGameStatistics GameStatistics { get; set; }
        public ITimerService Timer { get; set; }

        //Properties
        public int CurrentLevelIndex { get; set; }
        public bool IsLevelChangeEnabled { get; set; }
        public List<LevelInfo> LevelsData { get; set; }
        public LevelInfo CurrentLevel { get { return this.LevelsData[this.CurrentLevelIndex]; } }

        //Event
        public event EventHandler LevelChanged;

        //Constructor
        public LevelManager(ITimerService timerService, IGameStatistics gameStatistics)
        {
            //Services
            this.GameStatistics = gameStatistics;
            this.Timer = timerService;

            //Initialize
            this.IsLevelChangeEnabled = true;
            this.CurrentLevelIndex = 0;
            this.LevelsData = new List<LevelInfo>()
            {
                new LevelInfo() { Name = "0", FromCleanedLines = 0, Speed = TimeSpan.FromMilliseconds(1000), }, //First
                new LevelInfo() { Name = "1", FromCleanedLines = 10, Speed = TimeSpan.FromMilliseconds(1000), },
                new LevelInfo() { Name = "2", FromCleanedLines = 20, Speed = TimeSpan.FromMilliseconds(920), },
                new LevelInfo() { Name = "3", FromCleanedLines = 40, Speed = TimeSpan.FromMilliseconds(840), },
                new LevelInfo() { Name = "4", FromCleanedLines = 60, Speed = TimeSpan.FromMilliseconds(780), },
                new LevelInfo() { Name = "5", FromCleanedLines = 90, Speed = TimeSpan.FromMilliseconds(700), },
                new LevelInfo() { Name = "6", FromCleanedLines = 100, Speed = TimeSpan.FromMilliseconds(620), },
                new LevelInfo() { Name = "7", FromCleanedLines = 120, Speed = TimeSpan.FromMilliseconds(520), },
                new LevelInfo() { Name = "8", FromCleanedLines = 200, Speed = TimeSpan.FromMilliseconds(400), },
                new LevelInfo() { Name = "9", FromCleanedLines = Int32.MaxValue, Speed = TimeSpan.FromMilliseconds(400), }, //Last
            };
            this.CheckDifficultyLevel();
        }

        //Methods
        public void CheckDifficultyLevel()
        {
            LevelInfo currentLevel = this.LevelsData[this.CurrentLevelIndex];
            this.CurrentLevelIndex = this.LevelsData.FindIndex(level => level.FromCleanedLines >= this.GameStatistics.Statistics.CleanedLines);
            LevelInfo nextLevel = this.LevelsData[this.CurrentLevelIndex];
            this.Timer.SetInterval(nextLevel.Speed);

            if (currentLevel.Name != nextLevel.Name)
            {
                this.LevelChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        public void RaiseDifficultyLevel()
        {
            this.CurrentLevelIndex++;
            this.LevelChanged?.Invoke(this, EventArgs.Empty);
        }

        public void LowerDifficultyLevel()
        {
            this.CurrentLevelIndex--;
            this.LevelChanged?.Invoke(this, EventArgs.Empty);
        }
    }
}
