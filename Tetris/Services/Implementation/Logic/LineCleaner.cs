﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Model;
using Tetris.Services.Contract.Logic;

namespace Tetris.Services.Implementation.Logic
{
    public class LineCleaner : ILineCleaner
    {
        //Services
        public IConfiguration Configuration { get; set; }
        public IBoardHolder BoardHolder { get; set; }
        public IBlockFactory BlockFactory { get; set; }
        public IBoardChanger BoardChanger { get; set; }
        public IGameStatistics GameStatistics { get; set; }

        //Constructor
        public LineCleaner(IConfiguration configuration, IBoardChanger boardChanger, IBoardHolder boardHolder, IBlockFactory blockFactory,
            IGameStatistics gameStatistics)
        {
            //Services
            this.Configuration = configuration;
            this.BoardHolder = boardHolder;
            this.BlockFactory = blockFactory;
            this.BoardChanger = boardChanger;
            this.GameStatistics = gameStatistics;
        }

        //Methods
        public void CleanFullLines()
        {
            int numberCleanedOfLines = 0;

            for (int i = 0; i < this.Configuration.BoardHeightInBlocks; i++)
            {
                if (this.IsLineIsFull(i))
                {
                    this.CleanLine(i);
                    numberCleanedOfLines++;
                }
            }

            this.GameStatistics.AddPointsForCleanLines(numberCleanedOfLines);
        }

        public bool IsLineIsFull(int lineNumber)
        {
            IEnumerable<Block> lineBlocks = this.BoardHolder.Board.Blocks.Where(b => b.Y == lineNumber);
            foreach (Block block in lineBlocks)
            {
                if (this.BoardChanger.IsBlockEmpty(block.X, block.Y))
                {
                    return false;
                }
            }
            return true;
        }

        public void CleanLine(int lineNumber)
        {
            //Move down all block under deleted block
            for (int y = lineNumber - 1; y >= 0; y--)
            {
                for (int x = 0; x < this.Configuration.BoardWidthInBlocks; x++)
                {
                    Block block = this.BoardHolder.GetBlock(x, y);
                    this.BoardChanger.RemoveFromBoard(x, y);
                    this.BoardChanger.ChangeBlock(block, x, y + 1);
                }
            }

            //Create new empty line on top
            for (int i = 0; i < this.Configuration.BoardWidthInBlocks; i++)
            {
                Block empty = this.BlockFactory.CreateEmpty(i, 0);
                this.BoardChanger.ChangeBlock(empty, i, 0);
            }
        }
    }
}
