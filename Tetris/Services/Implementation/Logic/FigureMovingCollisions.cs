﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Model;
using Tetris.Services.Contract.Logic;

namespace Tetris.Services.Implementation.Logic
{
    public class FigureMovingCollisions : IFigureMovingCollisions
    {
        //Services
        public IConfiguration Configuration { get; set; }
        public IBoardHolder BoardHolder { get; set; }
        public IFigureHolder FigureHolder { get; set; }
        public IBoardChanger BoardChanger { get; set; }

        //Constructor
        public FigureMovingCollisions(IConfiguration configuration, IBoardHolder boardHolder, IFigureHolder figureHolder, IBoardChanger boardChanger)
        {
            //Services
            this.Configuration = configuration;
            this.BoardHolder = boardHolder;
            this.FigureHolder = figureHolder;
            this.BoardChanger = boardChanger;
        }

        //Methods
        public bool HasCollisionDown()
        {
            return HasCollisionDown(this.FigureHolder.FallingFigure);
        }

        public bool HasCollisionDown(Figure figure)
        {
            if (figure == null)
            {
                return true;
            }

            //Board border
            foreach (Block block in figure.Blocks)
            {
                if (figure.Y + block.Y >= this.Configuration.BoardHeightInBlocks - 1)
                {
                    return true;
                }
            }

            //Blocks
            foreach (Block block in figure.Blocks)
            {
                int blockX = figure.X + block.X;
                int blockY = figure.Y + block.Y;
                if (!this.BoardChanger.IsBlockEmpty(blockX, blockY + 1))
                {
                    return true;
                }
            }

            return false;
        }

        public bool IsOutsideTheBoardFromLeft()
        {
            return this.IsOutsideTheBoardFromLeft(this.FigureHolder.FallingFigure);
        }

        public bool IsOutsideTheBoardFromLeft(Figure figure)
        {
            foreach (Block block in figure.Blocks)
            {
                int blockPosX = figure.X + block.X;
                if (blockPosX < 0)
                {
                    return true;
                }
            }

            return false;
        }

        public bool HasCollisionLeft()
        {
            return this.HasCollisionLeft(this.FigureHolder.FallingFigure);
        }

        public bool HasCollisionLeft(Figure figure)
        {
            if (figure != null)
            {
                //Board border
                foreach (Block block in figure.Blocks)
                {
                    if (figure.X + block.X <= 0)
                    {
                        return true;
                    }
                }

                //Blocks
                foreach (Block block in figure.Blocks)
                {
                    int blockX = figure.X + block.X;
                    int blockY = figure.Y + block.Y;
                    if (!this.BoardChanger.IsBlockEmpty(blockX - 1, blockY))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public bool IsOutsideTheBoardFromRight()
        {
            return this.IsOutsideTheBoardFromRight(this.FigureHolder.FallingFigure);
        }

        public bool IsOutsideTheBoardFromRight(Figure figure)
        {
            foreach (Block block in figure.Blocks)
            {
                int blockPosX = figure.X + block.X;
                if (blockPosX > this.Configuration.BoardWidthInBlocks - 1)
                {
                    return true;
                }
            }

            return false;
        }

        public bool HasCollisionRight()
        {
            return this.HasCollisionRight(this.FigureHolder.FallingFigure);
        }

        public bool HasCollisionRight(Figure figure)
        {
            //Board border
            foreach (Block block in figure.Blocks)
            {
                if (figure.X + block.X >= this.Configuration.BoardWidthInBlocks - 1)
                {
                    return true;
                }
            }

            //Blocks
            foreach (Block block in figure.Blocks)
            {
                int blockX = figure.X + block.X;
                int blockY = figure.Y + block.Y;
                if (!this.BoardChanger.IsBlockEmpty(blockX + 1, blockY))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
