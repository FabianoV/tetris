﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Tetris.Model;
using Tetris.Services.Contract.Logic;

namespace Tetris.Services.Implementation.Logic
{
    public class FigureMoving : IFigureMoving
    {
        //Services
        public IFigureHolder FigureHolder { get; set; }
        public IFigureMovingCollisions Collisions { get; set; }
        public IBoardChanger BoardChanger { get; set; }
        public ILineCleaner LineClener { get; set; }
        public IFigureChanger FigureChanger { get; set; }

        //Events
        public event EventHandler<Figure> FigureMoveDown;
        public event EventHandler<Figure> FigureMoveLeft;
        public event EventHandler<Figure> FigureMoveRight;

        //Construcotr
        public FigureMoving(IFigureHolder figureHolder, IFigureMovingCollisions collisions, IBoardChanger boardChanger, 
            ILineCleaner lineCleaner, IFigureChanger figureChanger) 
        {
            //Services
            this.FigureHolder = figureHolder;
            this.Collisions = collisions;
            this.BoardChanger = boardChanger;
            this.LineClener = lineCleaner;
            this.FigureChanger = figureChanger;
        }

        //Methods
        public void MoveDown()
        {
            this.MoveDown(this.FigureHolder.FallingFigure);
        }

        public void MoveDown(Figure figure)
        {
            if (!Collisions.HasCollisionDown())
            {
                figure.Y++;
                this.FigureChanger.CalculateBlocksRelativePositions();
            }

            this.FigureMoveDown?.Invoke(this, figure);
        }

        public void MoveDownToBottom()
        {
            this.MoveDownToBottom(this.FigureHolder.FallingFigure);
        }

        public void MoveDownToBottom(Figure figure)
        {
            while (!Collisions.HasCollisionDown())
            {
                figure.Y++;
                this.FigureChanger.CalculateBlocksRelativePositions();
            }

            this.FigureMoveDown?.Invoke(this, figure);
        }

        public void MoveLeft()
        {
            this.MoveLeft(this.FigureHolder.FallingFigure);
        }

        public void MoveLeft(Figure figure)
        {
            if (!Collisions.HasCollisionLeft(figure))
            {
                figure.X--;
                this.FigureChanger.CalculateBlocksRelativePositions(figure);
            }

            this.FigureMoveLeft?.Invoke(this, figure);
        }

        public void MoveRight()
        {
            this.MoveRight(this.FigureHolder.FallingFigure);
        }

        public void MoveRight(Figure figure)
        {
            if (!Collisions.HasCollisionRight(figure))
            {
                figure.X++;
                this.FigureChanger.CalculateBlocksRelativePositions(figure);
            }

            this.FigureMoveRight?.Invoke(this, figure);
        }
    }
}
