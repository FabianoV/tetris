﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Services.Contract.Achievments;
using Tetris.Services.Contract.Logic;

namespace Tetris.Services.Implementation.Logic
{
    public class GameComponents : IGameComponents
    {
        //Services
        public IConfiguration Configuration { get; set; }
        public ITimerService Timer { get; set; }
        public IBoardHolder BoardHolder { get; set; }
        public IBoardFactory BoardFactory { get; set; }
        public IFigureMovingCollisions Collisions { get; set; }
        public IFigureFactory FigureFactory { get; set; }
        public IFigureHolder FigureHolder { get; set; }
        public IFigurePutter FigurePutter { get; set; }
        public IGameDefeatChecker GameDefeatChecker { get; set; }
        public ILineCleaner LineCleaner { get; set; }
        public IFigureMoving FigureMoving { get; set; }
        public IGameStatistics GameStatistics { get; set; }
        public ILevelManager LevelManager { get; set; }
        public IGameControl GameControl { get; set; }
        public IGameLoop GameLoop { get; set; }
        public IAchievementsDataHolder AchievementsDataHolder { get; set; }

        //Constructor
        public GameComponents(IConfiguration configuration, ITimerService timer, IBoardHolder boardHolder, IBoardFactory boardFactory,
            IFigureFactory figureFactory, IFigureHolder figureHolder, IFigurePutter figurePutter, IGameDefeatChecker gameDefeatChecker,
             ILineCleaner lineCleaner, IFigureMoving figureMoving, IFigureMovingCollisions collisions, IGameStatistics gameStatistics,
             ILevelManager levelManager, IGameControl gameControl, IGameLoop gameLoop, IAchievementsDataHolder achievementsDataHolder)
        {
            //Services
            this.Configuration = configuration;
            this.BoardHolder = boardHolder;
            this.BoardFactory = boardFactory;
            this.Timer = timer;
            this.Collisions = collisions;
            this.FigureFactory = figureFactory;
            this.FigureHolder = figureHolder;
            this.FigurePutter = figurePutter;
            this.GameDefeatChecker = gameDefeatChecker;
            this.LineCleaner = lineCleaner;
            this.FigureMoving = figureMoving;
            this.GameStatistics = gameStatistics;
            this.LevelManager = levelManager;
            this.GameControl = gameControl;
            this.GameLoop = gameLoop;
            this.AchievementsDataHolder = achievementsDataHolder;
        }
    }
}
