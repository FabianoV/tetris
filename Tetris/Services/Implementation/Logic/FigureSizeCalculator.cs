﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Model;
using Tetris.Services.Contract.Logic;

namespace Tetris.Services.Implementation.Logic
{
    public class FigureSizeCalculator : IFigureSizeCalculator
    {
        public int CalculateHeightInBlocks(Figure figure)
        {
            int topBlockY = figure.Blocks.Min(block => block.Y);
            int bottomBlockY = figure.Blocks.Max(block => block.Y);
            return bottomBlockY - topBlockY + 1;
        }

        public int CalculateWidthInBlocks(Figure figure)
        {
            int leftBlockX = figure.Blocks.Min(block => block.X);
            int rightBlockX = figure.Blocks.Max(block => block.X);
            return rightBlockX - leftBlockX + 1;
        }
    }
}
