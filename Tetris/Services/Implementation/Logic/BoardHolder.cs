﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Model;
using Tetris.Services.Contract.Logic;

namespace Tetris.Services.Implementation.Logic
{
    public class BoardHolder : IBoardHolder
    {
        public Board Board { get; set; }

        public Block GetBlock(int x, int y)
        {
            return this.Board.Blocks.FirstOrDefault(b => b.X == x && b.Y == y);
        }

        public IEnumerable<Block> GetLine(int y)
        {
            return this.Board.Blocks.Where(b => b.Y == y);
        }
    }
}
