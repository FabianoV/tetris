﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Model;
using Tetris.Services.Contract.Logic;

namespace Tetris.Services.Implementation.Logic
{
    public class BoardChanger : IBoardChanger
    {
        //Services
        public IBoardHolder BoardHolder { get; set; }
        public IBlockTexturesService BlockTextures { get; set; }

        //Constructor
        public BoardChanger(IBoardHolder boardHolder, IBlockTexturesService blockTextures)
        {
            //Services
            this.BoardHolder = boardHolder;
            this.BlockTextures = blockTextures;
        }

        //Methods
        public void ChangeBlock(Block newBlock, int x, int y)
        {
            this.RemoveFromBoard(x, y);
            newBlock.X = x;
            newBlock.Y = y;
            this.BoardHolder.Board.Blocks.Add(newBlock);
        }

        public void RemoveFromBoard(int x, int y)
        {
            Block toRemove = this.BoardHolder.Board.Blocks.FirstOrDefault(b => b.X == x && b.Y == y);
            if (toRemove != null)
            {
                this.BoardHolder.Board.Blocks.Remove(toRemove);
            }
        }

        public bool IsBlockEmpty(int x, int y)
        {
            List<Block> blocks = this.BoardHolder.Board.Blocks;
            Block block = blocks.FirstOrDefault(b => b.X == x && b.Y == y);

            if (block.Texture.Equals(this.BlockTextures.EmptyBlockTexture))
            {
                return true;
            }
            return false;
        }
    }
}
