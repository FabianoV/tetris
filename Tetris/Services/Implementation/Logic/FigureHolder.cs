﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using Tetris.Model;
using Tetris.Model.Figures;
using Tetris.Services.Contract.Logic;

namespace Tetris.Services.Implementation.Logic
{
    public class FigureHolder : IFigureHolder
    {
        //Services
        public IBlockTexturesService BlockTextures { get; set; }

        //Properties
        public Figure FallingFigure { get; set; }
        public Queue<Figure> NextFigures { get; set; }
        public List<Type> FiguresTypes { get; set; }
        public Dictionary<Type, BitmapImage> FigureTextures { get; set; }
        public Figure StoredFigure { get; set; }

        //Costructor
        public FigureHolder(IBlockTexturesService blockTextures)
        {
            //Services
            this.BlockTextures = blockTextures;

            //Initialize
            this.FiguresTypes = new List<Type>() { typeof(I), typeof(J), typeof(L), typeof(O), typeof(S), typeof(T), typeof(Z) };
            this.FigureTextures = new Dictionary<Type, BitmapImage>()
            {
                { typeof(I), this.BlockTextures.BlueBlockTexture },
                { typeof(J), this.BlockTextures.OrangeBlockTexture },
                { typeof(L), this.BlockTextures.DarkBlueBlockTexture },
                { typeof(O), this.BlockTextures.YellowBlockTexture },
                { typeof(S), this.BlockTextures.RedBlockTexture },
                { typeof(T), this.BlockTextures.PurpleBlockTexture },
                { typeof(Z), this.BlockTextures.GreenBlockTexture },
            };
            this.Reset();
        }

        //Methods
        public void Reset()
        {
            this.FallingFigure = null;
            this.NextFigures = new Queue<Figure>();
            this.StoredFigure = null;
        }

        public void LoadState(Figure figure)
        {
            this.FallingFigure = figure;

        }
    }
}
