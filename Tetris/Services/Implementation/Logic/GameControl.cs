﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Tetris.Services.Contract;
using Tetris.Services.Contract.Drawing;
using Tetris.Services.Contract.Logic;
using Tetris.ViewModel;

namespace Tetris.Services.Implementation.Logic
{
    public class GameControl : IGameControl
    {
        //Services
        public IGameEngine GameEngine { get; set; }
        public IFigureMoving FigureMoving { get; set; }
        public IFigureFactory FigureFactory { get; set; }
        public IFigureRotation FigureRotation { get; set; }
        public IFigurePutter FigurePutter { get; set; }
        public ILineCleaner LineCleaner { get; set; }
        public ILevelManager LevelManager { get; set; }
        public IFigureStoreManager FigureStoreManager { get; set; }
        public IBoardHolder BoardHolder { get; set; }

        //Constructor
        public GameControl(IFigureFactory figureFactory, IFigurePutter figurePutter, ILineCleaner lineCleaner,
            IFigureRotation figureRotation, IFigureMoving figureMoving, ILevelManager levelManager, IBoardHolder boardHolder, 
            IFigureStoreManager figureStoreManager, IGameEngine gameEngine)
        {
            //Services
            this.GameEngine = gameEngine;
            this.FigureFactory = figureFactory;
            this.FigurePutter = figurePutter;
            this.LineCleaner = lineCleaner;
            this.FigureMoving = figureMoving;
            this.LevelManager = levelManager;
            this.BoardHolder = boardHolder;
            this.FigureRotation = figureRotation;
            this.FigureStoreManager = figureStoreManager;
        }

        //Methods
        public void SendKey(Key key)
        {
            if (this.GameEngine.Active)
            {
                switch (key)
                {
                    case Key.Right:
                    case Key.D: this.FigureMoving.MoveRight(); break;
                    case Key.Left:
                    case Key.A: this.FigureMoving.MoveLeft(); break;
                    case Key.Down:
                    case Key.S: this.FigureMoving.MoveDown(); break;
                    case Key.F: this.FigureRotation.RotateLeft(); break;
                    case Key.G: this.FigureRotation.RotateRight(); break;
                    case Key.P: this.GameEngine.Pause(); break;
                    case Key.E:
                        this.FigureStoreManager.StoreFallingFigure();
                        this.RedrawGame();
                        break;
                    case Key.Space:
                        this.FigureMoving.MoveDownToBottom();
                        this.FigurePutter.PutFallingFigureOnBoard();
                        this.LineCleaner.CleanFullLines();
                        this.LevelManager.CheckDifficultyLevel();
                        this.RedrawGame();
                        break;
                    case Key.Escape: this.GameEngine.Pause(); break;
                }
            }
        }
        private void RedrawGame()
        {
            this.GameEngine.RedrawGame();
        }
    }
}
