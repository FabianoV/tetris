﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Threading;
using Tetris.Services.Contract.Logic;

namespace Tetris.Services.Implementation.Logic
{
    public class TimerService : ITimerService
    {
        //Services
        public IConfiguration Configuration { get; set; }

        //Properties
        public DispatcherTimer Timer { get; set; }
        public event EventHandler TimerTick;

        //Cosntructor
        public TimerService(IConfiguration configuration)
        {
            //Services
            this.Configuration = configuration;

            //Initialize
            this.Timer = new DispatcherTimer();
            this.Timer.Interval = TimeSpan.FromMilliseconds(this.Configuration.TimerSpeed);
            this.Timer.Tick += TimerService_TimerTick;
        }

        public void Start()
        {
            this.Timer.Start();
        }

        public void Stop()
        {
            Timer.Stop();
        }

        public void SetInterval(TimeSpan interval)
        {
            this.Timer.Interval = interval;
        }

        private void TimerService_TimerTick(object sender, EventArgs e)
        {
            this.TimerTick?.Invoke(this, EventArgs.Empty);
        }
    }
}
