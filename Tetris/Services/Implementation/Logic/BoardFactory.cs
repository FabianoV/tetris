﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Model;
using Tetris.Services.Contract.Logic;

namespace Tetris.Services.Implementation.Logic
{
    public class BoardFactory : IBoardFactory
    {
        //Services
        public IBlockFactory BlockFactory { get; set; }

        //Constructor
        public BoardFactory(IBlockFactory blockFactory)
        {
            this.BlockFactory = blockFactory;
        }

        //Methods
        public Board Create(int width, int height)
        {
            this.ValidSize(width, height);

            Board board = new Board()
            {
                Height = height,
                Width = width,
                Blocks = new List<Block>(),
            };

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    Block b = this.BlockFactory.CreateEmpty(x, y);
                    board.Blocks.Add(b);
                }
            }

            return board;
        }

        public void ValidSize(int width, int height)
        {
            int MinimumBoardWidth = 8;
            int MinimumBoardHeight = 8;

            if (width < MinimumBoardWidth)
            {
                throw new ArgumentException("Board width too small.");
            }

            if (height < MinimumBoardHeight)
            {
                throw new ArgumentException("Board height too small.");
            }
        }
    }
}
