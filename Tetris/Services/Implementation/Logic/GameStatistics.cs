﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Model.Tetris;
using Tetris.Services.Contract.Logic;
using Tetris.Services.Contract.Achievments;

namespace Tetris.Services.Implementation.Logic
{
    public class GameStatistics : IGameStatistics
    {
        //Services
        public IAchievmentsDataCache AchievmentStatistics { get; set; }
        public IAchievementsDataHolder AchievmentsHolder { get; set; }

        //Properties
        public CurrentGameStatistics Statistics { get; set; }

        //Events
        public event EventHandler PointCounterChanged;

        //Constructor
        public GameStatistics(IAchievmentsDataCache achivmentStatistics, IAchievementsDataHolder achievmentHolder)
        {
            //Services
            this.AchievmentStatistics = achivmentStatistics;
            this.AchievmentsHolder = achievmentHolder;

            //Initialize
            this.Reset();
        }

        //Methods
        public void AddPointsForCleanLines(int numberOfLines)
        {
            this.Statistics.CleanedLines += numberOfLines;
            this.AchievmentStatistics.Statistics.CleanedLines += numberOfLines;

            int pointsToAdd = 0;
            switch (numberOfLines)
            {
                case 0: break;
                case 1:
                    pointsToAdd += 100;
                    this.Statistics.SingleLine++;
                    this.AchievmentStatistics.Statistics.SingleLine++;
                    break;
                case 2:
                    pointsToAdd += 220;
                    this.Statistics.DoubleLine++;
                    this.AchievmentStatistics.Statistics.DoubleLine++;
                    break;
                case 3:
                    pointsToAdd += 350;
                    this.Statistics.TripleLine++;
                    this.AchievmentStatistics.Statistics.TripleLine++;
                    break;
                case 4:
                    pointsToAdd += 500;
                    this.AchievmentStatistics.Statistics.Tetris++;
                    this.Statistics.Tetris++; break;
                default:
                    throw new Exception("Wrong number of lines. Lines count must be in <0, 4>.");
            }
            this.Statistics.Points += pointsToAdd;
            this.AchievmentStatistics.Statistics.Points += pointsToAdd;

            this.AchievmentsHolder.CheckAchievmentsNowReached(this.Statistics);

            this.PointCounterChanged?.Invoke(this, EventArgs.Empty);
        }

        public void Reset()
        {
            this.Statistics = new CurrentGameStatistics()
            {
                GameStart = DateTime.Now,

                CleanedLines = 0,
                Points = 0,

                SingleLine = 0,
                DoubleLine = 0,
                TripleLine = 0,
                Tetris = 0,
            };

            this.PointCounterChanged?.Invoke(this, EventArgs.Empty);
        }
    }
}
