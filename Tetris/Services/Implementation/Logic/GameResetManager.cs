﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Services.Contract.Logic;

namespace Tetris.Services.Implementation.Logic
{
    public class GameResetManager : IGameResetManager
    {
        //Services
        public IConfiguration Configuration { get; set; }
        public ITimerService Timer { get; set; }
        public IBoardHolder BoardHolder { get; set; }
        public IBoardFactory BoardFactory { get; set; }
        public IFigureMovingCollisions Collisions { get; set; }
        public IFigureFactory FigureFactory { get; set; }
        public IFigureHolder FigureHolder { get; set; }
        public IFigurePutter FigurePutter { get; set; }
        public IGameDefeatChecker GameDefeatChecker { get; set; }
        public ILineCleaner LineCleaner { get; set; }
        public IFigureMoving FigureMoving { get; set; }
        public IGameStatistics GameStatistics { get; set; }
        public ILevelManager LevelManager { get; set; }

        //Constructor
        public GameResetManager(IConfiguration configuration, ITimerService timer, IBoardHolder boardHolder, IBoardFactory boardFactory,
            IFigureFactory figureFactory, IFigureHolder figureHolder, IFigurePutter figurePutter, IGameDefeatChecker gameDefeatChecker,
             ILineCleaner lineCleaner, IFigureMoving figureMoving, IFigureMovingCollisions collisions, IGameStatistics gameStatistics,
             ILevelManager levelManager)
        {
            //Services
            this.Configuration = configuration;
            this.BoardHolder = boardHolder;
            this.BoardFactory = boardFactory;
            this.Timer = timer;
            this.Collisions = collisions;
            this.FigureFactory = figureFactory;
            this.FigureHolder = figureHolder;
            this.FigurePutter = figurePutter;
            this.GameDefeatChecker = gameDefeatChecker;
            this.LineCleaner = lineCleaner;
            this.FigureMoving = figureMoving;
            this.GameStatistics = gameStatistics;
            this.LevelManager = levelManager;
        }

        //Methods
        public void ResetGame()
        {
            //Stop Game
            this.Timer.Stop();

            //Regenerate Board
            this.BoardHolder.Board = this.BoardFactory.Create(this.Configuration.BoardWidthInBlocks, this.Configuration.BoardHeightInBlocks);

            //Delete Figures
            this.FigureHolder.FallingFigure = null;
            this.FigureHolder.NextFigures = new Queue<Model.Figure>();
            this.FigureHolder.StoredFigure = null;

            //Reset Statistics and Score
            this.GameStatistics.Reset();
        }
    }
}
