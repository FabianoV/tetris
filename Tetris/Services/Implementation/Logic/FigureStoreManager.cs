﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Model;
using Tetris.Services.Contract.Logic;

namespace Tetris.Services.Implementation.Logic
{
    public class FigureStoreManager : IFigureStoreManager
    {
        //Services
        public IConfiguration Configuration { get; set; }
        public IFigureHolder FigureHolder { get; set; }
        public IFigureFactory FigureFactory { get; set; }

        //Properties
        public bool IsFigureStored { get; set; }
        public bool CanStoreFigure { get; set; }

        //Constructor
        public FigureStoreManager(IConfiguration configuration, IFigureHolder figureHolder, IFigureFactory figureFactory)
        {
            //Services
            this.Configuration = configuration;
            this.FigureHolder = figureHolder;
            this.FigureFactory = figureFactory;

            //Initialize
            this.IsFigureStored = false;
            this.CanStoreFigure = true;
        }

        //Methods
        public void StoreFallingFigure()
        {
            if (!CanStoreFigure)
            {
                return;
            }

            if (this.FigureHolder.FallingFigure == null)
            {
                return;
            }

            if (this.IsFigureStored)
            {
                Figure temp = this.FigureHolder.FallingFigure;
                this.FigureHolder.FallingFigure = this.FigureHolder.StoredFigure;
                this.FigureHolder.StoredFigure = temp;
            }
            else
            {
                this.FigureHolder.StoredFigure = this.FigureHolder.FallingFigure;
                this.FigureFactory.Create();
            }

            this.FigureHolder.StoredFigure.X = this.Configuration.FigureStartingPositionX;
            this.FigureHolder.StoredFigure.Y = this.Configuration.FigureStartingPositionY;

            this.IsFigureStored = true;
            this.CanStoreFigure = false;
        }

        public void Reset()
        {
            this.IsFigureStored = false;
            this.CanStoreFigure = true;
        }
    }
}
