﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Services.Contract.Logic;

namespace Tetris.Services.Implementation.Logic
{
    public class GameDefeatChecker : IGameDefeatChecker
    {
        //Services
        public IFigureHolder FigureHolder { get; set; }
        public IFigureMovingCollisions Collisions { get; set; }

        //Properties
        public bool IsGameEnded { get; set; }

        //Constructor
        public GameDefeatChecker(IFigureHolder figureHolder, IFigureMovingCollisions collisions)
        {
            //Services
            this.FigureHolder = figureHolder;
            this.Collisions = collisions;
        }

        //Methods
        public bool CalculateIsGameEnded()
        {
            //Can check only after figure created
            if (this.Collisions.HasCollisionDown())
            {
                return true;
            }

            return false;
        }

        public void Reset()
        {
            this.IsGameEnded = false;
        }
    }
}
