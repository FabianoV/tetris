﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using Tetris.Model;
using Tetris.Services.Contract.Logic;

namespace Tetris.Services.Implementation.Logic
{
    public class BlockFactory : IBlockFactory
    {
        //Services
        public IBlockTexturesService BlockTextureService { get; set; }

        //Constructor
        public BlockFactory(IBlockTexturesService blockTextureService)
        {
            this.BlockTextureService = blockTextureService;
        }

        //Methods
        public Block Create(BitmapImage texture, int x, int y)
        {
            Block block = new Block()
            {
                X = x,
                Y = y,
                Texture = texture,
            };

            return block;
        }

        public Block Create(string textureName, int x, int y)
        {
            BitmapImage texture = this.BlockTextureService.TextureNameToBitmapImage(textureName);
            return this.Create(texture, x, y);
        }

        public Block CreateEmpty(int x, int y)
        {
            Block block = new Block()
            {
                X = x,
                Y = y,
                Texture = this.BlockTextureService.EmptyBlockTexture,
            };

            return block;
        }
    }
}
