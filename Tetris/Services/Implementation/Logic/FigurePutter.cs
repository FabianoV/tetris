﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Model;
using Tetris.Services.Contract.Logic;

namespace Tetris.Services.Implementation.Logic
{
    public class FigurePutter : IFigurePutter
    {
        //Services
        public IFigureHolder FigureHolder { get; set; }
        public IFigureFactory FigureFactory { get; set; }
        public IBoardChanger BoardChanger { get; set; }
        public IFigureMovingCollisions Collisions { get; set; }
        public IFigureMoving FigureMoving { get; set; }
        public IFigureStoreManager FigureStoreManager { get; set; }

        //Event
        public event EventHandler<Figure> FigurePuttedOnBoard;

        //Constructor
        public FigurePutter(IFigureHolder figureHolder, IBoardChanger boardChanger, IFigureMovingCollisions collisions, 
            IFigureMoving figureMoving, IFigureFactory figureFactory, IFigureStoreManager figureStoreManager)
        {
            //Services
            this.FigureHolder = figureHolder;
            this.BoardChanger = boardChanger;
            this.Collisions = collisions;
            this.FigureMoving = figureMoving;
            this.FigureFactory = figureFactory;
            this.FigureStoreManager = figureStoreManager;
        }

        //Methods
        public void PutFallingFigureOnBoard()
        {
            if (this.FigureHolder.FallingFigure == null)
            {
                return;
            }

            //Add last figure to board
            Figure lastFigure = this.FigureHolder.FallingFigure;
            foreach (Block block in lastFigure.Blocks)
            {
                int blockX = lastFigure.X + block.X;
                int blockY = lastFigure.Y + block.Y;
                this.BoardChanger.ChangeBlock(block, blockX, blockY);
            }

            this.FigureFactory.Create();

            this.FigureStoreManager.CanStoreFigure = true;

            this.FigurePuttedOnBoard?.Invoke(this, lastFigure);
        }
    }
}
