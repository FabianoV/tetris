﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using Tetris.Model.Tetris.Textures;
using Tetris.Services.Contract.Logic;

namespace Tetris.Services.Implementation.Logic
{
    public class BlockTexturesService : IBlockTexturesService
    {
        //Consts
        public const string EmptyBlockTextureName = "Empty";
        public const string YellowBlockTextureName = "Yellow";
        public const string BlueBlockTextureName = "Blue";
        public const string RedBlockTextureName = "Red";
        public const string GreenBlockTextureName = "Green";
        public const string PurpleBlockTextureName = "Purple";
        public const string OrangeBlockTextureName = "Orange";
        public const string DarkBlueBlockTextureName = "DarkBlue";

        //Properties
        public BitmapImage EmptyBlockTexture { get; set; }
        public BitmapImage YellowBlockTexture { get; set; }
        public BitmapImage BlueBlockTexture { get; set; }
        public BitmapImage RedBlockTexture { get; set; }
        public BitmapImage GreenBlockTexture { get; set; }
        public BitmapImage PurpleBlockTexture { get; set; }
        public BitmapImage OrangeBlockTexture { get; set; }
        public BitmapImage DarkBlueBlockTexture { get; set; }

        string emptyBlockTexturePath = @"Content\empty_block_background.png";
        string yellowBlockTexturePath = @"Content\yellow_block_background.png";
        string blueBlockTexturePath = @"Content\blue_block_bacground.png";
        string redBlockTexturePath = @"Content\red_block_background.png";
        string greenBlockTexturePath = @"Content\green_block_background.png";
        string purpleBlockTexturePath = @"Content\purple_block_background.png";
        string orangeBlockTexturePath = @"Content\orange_block_background.png";
        string darkBlueBlockTexturePath = @"Content\dark_blue_block_background.png";

        public bool IsTexturesLoaded { get; set; }
        public string StartupPath { get; set; }
        public List<TextureData> TexturesData { get; set; }

        //Constructor
        public BlockTexturesService()
        {
            this.IsTexturesLoaded = false;
            this.StartupPath = AppDomain.CurrentDomain.BaseDirectory;
            this.LoadTextures();
        }

        //Methods
        public void LoadTextures()
        {
            this.EmptyBlockTexture = new BitmapImage(new Uri(Path.Combine(StartupPath, emptyBlockTexturePath)));
            this.YellowBlockTexture = new BitmapImage(new Uri(Path.Combine(StartupPath, yellowBlockTexturePath)));
            this.BlueBlockTexture = new BitmapImage(new Uri(Path.Combine(StartupPath, blueBlockTexturePath)));
            this.RedBlockTexture = new BitmapImage(new Uri(Path.Combine(StartupPath, redBlockTexturePath)));
            this.GreenBlockTexture = new BitmapImage(new Uri(Path.Combine(StartupPath, greenBlockTexturePath)));
            this.PurpleBlockTexture = new BitmapImage(new Uri(Path.Combine(StartupPath, purpleBlockTexturePath)));
            this.OrangeBlockTexture = new BitmapImage(new Uri(Path.Combine(StartupPath, orangeBlockTexturePath)));
            this.DarkBlueBlockTexture = new BitmapImage(new Uri(Path.Combine(StartupPath, darkBlueBlockTexturePath)));

            this.TexturesData = new List<TextureData>()
            {
                new TextureData() { Name = EmptyBlockTextureName, Path = this.emptyBlockTexturePath, Texture = this.EmptyBlockTexture },
                new TextureData() { Name = YellowBlockTextureName, Path = this.yellowBlockTexturePath, Texture = this.YellowBlockTexture},
                new TextureData() { Name = BlueBlockTextureName, Path = this.blueBlockTexturePath, Texture = this.BlueBlockTexture },
                new TextureData() { Name = RedBlockTextureName, Path = this.redBlockTexturePath, Texture = this.RedBlockTexture },
                new TextureData() { Name = GreenBlockTextureName, Path = this.greenBlockTexturePath, Texture = this.GreenBlockTexture },
                new TextureData() { Name = PurpleBlockTextureName, Path = this.purpleBlockTexturePath, Texture = this.PurpleBlockTexture },
                new TextureData() { Name = OrangeBlockTextureName, Path = this.orangeBlockTexturePath, Texture = this.OrangeBlockTexture },
                new TextureData() { Name = DarkBlueBlockTextureName, Path = this.darkBlueBlockTexturePath, Texture = this.DarkBlueBlockTexture },
            };

            this.IsTexturesLoaded = true;
        }

        public BitmapImage TextureNameToBitmapImage(string textureName)
        {
            TextureData result = this.TexturesData.Where(td => td.Name == textureName).FirstOrDefault();
            if (result == null)
            {
                throw new NotImplementedException("Block type name not supported");
            }
            return result.Texture;
        }

        public string BitmapImageToTextureName(BitmapImage bitmap)
        {
            TextureData result = this.TexturesData.Where(td => td.Texture.Equals(bitmap)).FirstOrDefault();
            if (result == null)
            {
                throw new NotImplementedException("This bitmap instance is not in the collection of textures");
            }
            return result.Name;
        }
    }
}
