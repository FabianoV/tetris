﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Tetris.Model;
using Tetris.Services.Contract.Logic;

namespace Tetris.Services.Implementation.Logic
{
    public class FigureChanger : IFigureChanger
    {
        //Service
        public IFigureHolder FigureHolder { get; set; }

        //constructor
        public FigureChanger(IFigureHolder figureHolder)
        {
            //Services
            this.FigureHolder = figureHolder;
        }

        //Methods
        public void CalculateBlocksRelativePositions()
        {
            this.CalculateBlocksRelativePositions(this.FigureHolder.FallingFigure);
        }

        public Figure CalculateBlocksRelativePositions(Figure figure)
        {
            if (figure.Angle < 0) { figure.Angle += 360; }
            figure.Angle = figure.Angle % 360;
            this.ValidAngle(figure.Angle);

            if (figure.Angle == 0)
            {
                figure.CurrentRotationData = figure.Position0;
            }
            else if (figure.Angle == 90)
            {
                figure.CurrentRotationData = figure.Position90;
            }
            else if (figure.Angle == 180)
            {
                figure.CurrentRotationData = figure.Position180;
            }
            else if (figure.Angle == 270)
            {
                figure.CurrentRotationData = figure.Position270;
            }
            else
            {
                throw new ArgumentException("Figure angle is not valid.");
            }

            for (int i = 0; i < figure.NumberOfBlocks; i++)
            {
                Block current = figure.Blocks[i];
                current.X = (int)figure.CurrentRotationData[i].X;
                current.Y = (int)figure.CurrentRotationData[i].Y;
            }

            return figure;
        }

        private void ValidAngle(int angle)
        {
            if (angle % 90 != 0)
            {
                throw new ArgumentException("Angle must be 90x multiplicity number.");
            }
        }
    }
}
