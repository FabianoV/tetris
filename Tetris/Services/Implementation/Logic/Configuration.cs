﻿using Tetris.Services.Contract.Logic;

namespace Tetris.Services.Implementation.Logic
{
    public class Configuration : IConfiguration
    {
        int boardWidthInBlocks = 10;
        int boardHeightInBlocks = 20;

        int timerSpeed = 1000;

        public int BoardWidthInBlocks { get { return boardWidthInBlocks; } set { boardWidthInBlocks = value; } }
        public int BoardHeightInBlocks { get { return boardHeightInBlocks; } set { boardHeightInBlocks = value; } }

        public int FigureStartingPositionX { get { return this.BoardWidthInBlocks / 2 - 2; } }
        public int FigureStartingPositionY { get { return 0; } }
        
        public int TimerSpeed { get { return timerSpeed; } set { timerSpeed = value; } }
    }
}
