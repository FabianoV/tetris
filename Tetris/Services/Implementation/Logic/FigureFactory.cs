﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading.Tasks;
using Tetris.Model;
using Tetris.Model.Figures;
using Tetris.Services.Contract.Logic;

namespace Tetris.Services.Implementation.Logic
{
    public class FigureFactory : IFigureFactory
    {
        //Services
        public IConfiguration Configuration { get; set; }
        public IFigureHolder FigureHolder { get; set; }
        public IBlockFactory BlockFactory { get; set; }
        public IFigureChanger FigureChanger { get; set; }

        //Properties
        public Random Random { get; set; }

        //Events
        public event EventHandler<Figure> FigureCreated;

        //Constructor
        public FigureFactory(IConfiguration configuration, IFigureHolder figureHolder, IBlockFactory blockFactory, 
            IFigureChanger figureChanger)
        {
            //Services
            this.Configuration = configuration;
            this.FigureHolder = figureHolder;
            this.BlockFactory = blockFactory;
            this.FigureChanger = figureChanger;

            //Initialize
            this.Random = new Random();
        }

        //Methods
        public Figure Create()
        {
            while (this.FigureHolder.NextFigures.Count < 6)
            {
                Figure f = this.CreateEmpty(this.RandomFigure());
                this.FigureHolder.NextFigures.Enqueue(f);
            }

            this.FigureHolder.FallingFigure = this.FigureHolder.NextFigures.Dequeue();
            return this.FigureHolder.FallingFigure;
        }

        public Figure Create(Type type)
        {
            Figure created = null;

            do
            {
                created = this.CreateEmpty(type);
                this.FigureHolder.NextFigures.Enqueue(created);
            } while (this.FigureHolder.NextFigures.Count < 6);

            return created;
        }

        public Figure CreateEmpty(Type type)
        {
            Figure figure = (Figure)Activator.CreateInstance(type);
            figure.Texture = this.FigureHolder.FigureTextures[figure.GetType()];
            figure.X = this.Configuration.FigureStartingPositionX;
            figure.Y = this.Configuration.FigureStartingPositionY;

            figure.Blocks = new List<Block>();
            for (int i = 0; i < figure.NumberOfBlocks; i++)
            {
                Block block = this.BlockFactory.Create(figure.Texture, 0, 0);
                figure.Blocks.Add(block);
            }
            this.FigureChanger.CalculateBlocksRelativePositions(figure);

            this.FigureCreated?.Invoke(this, figure);

            return figure;
        }

        private Type RandomFigure()
        {
            int figueIndex = this.Random.Next(this.FigureHolder.FiguresTypes.Count);
            Type figureType = this.FigureHolder.FiguresTypes[figueIndex];
            return figureType;
        }
    }
}
