﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Services.Contract.Logic;

namespace Tetris.Services.Implementation.Logic
{
    public class GameLoop : IGameLoop
    {
        //Services
        public IGameEngine GameEngine { get; set; }
        public IFigureMovingCollisions Collisions { get; set; }
        public IFigurePutter FigurePutter { get; set; }
        public IGameDefeatChecker GameDefeatChecker { get; set; }
        public ILineCleaner LineCleaner { get; set; }
        public IFigureMoving FigureMoving { get; set; }
        public ILevelManager LevelManager { get; set; }
        public ITimerService Timer { get; set; }

        //Constructor
        public GameLoop(IGameEngine gameEngine, ITimerService timer, IFigurePutter figurePutter, IGameDefeatChecker gameDefeatChecker, 
            ILineCleaner lineCleaner, IFigureMoving figureMoving, IFigureMovingCollisions collisions, ILevelManager levelManager)
        {
            //Services
            this.GameEngine = gameEngine;
            this.Timer = timer;
            this.Collisions = collisions;
            this.FigurePutter = figurePutter;
            this.GameDefeatChecker = gameDefeatChecker;
            this.LineCleaner = lineCleaner;
            this.FigureMoving = figureMoving;
            this.LevelManager = levelManager;

            //Initialize
            this.Timer.TimerTick += TimerService_TimerTick;
        }

        //Methods
        private void TimerService_TimerTick(object sender, EventArgs e)
        {
            this.GameLoopLogic();
        }

        private void GameLoopLogic()
        {
            if (this.GameEngine.Active)
            {
                if (!Collisions.HasCollisionDown())
                {
                    this.FigureMoving.MoveDown();
                }
                else
                {
                    this.FigurePutter.PutFallingFigureOnBoard();
                    this.LineCleaner.CleanFullLines();
                    this.LevelManager.CheckDifficultyLevel();

                    if (this.GameDefeatChecker.CalculateIsGameEnded())
                    {
                        this.GameEngine.EndGame();
                    }
                }

                this.GameEngine.RedrawGame();
            }
        }
    }
}
