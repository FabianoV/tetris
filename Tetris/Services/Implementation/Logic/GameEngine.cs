﻿using GalaSoft.MvvmLight.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Model;
using Tetris.Model.Figures;
using Tetris.Services.Contract;
using Tetris.Services.Contract.Other;
using Tetris.Services.Contract.Achievments;
using Tetris.Services.Contract.Logic;

namespace Tetris.Services.Implementation.Logic
{
    public class GameEngine : IGameEngine
    {
        //Game Services
        public IConfiguration Configuration { get; set; }
        public ITimerService Timer { get; set; }
        public IBoardHolder BoardHolder { get; set; }
        public IBoardFactory BoardFactory { get; set; }
        public IFigureFactory FigureFactory { get; set; }
        public IFigureHolder FigureHolder { get; set; }
        public IGameDefeatChecker GameDefeatChecker { get; set; }
        public IGameStatistics GameStatistics { get; set; }
        public IAchievmentsDataCache AchievmentsCache { get; set; }
        public IGameTimeCalculator GameTimeCalculator { get; set; }
        public IFigureStoreManager FigureStore { get; set; }
        public IGameComponents GameComponents { get; set; }

        //Properties
        public bool Active { get; set; }
        public bool IsPaused { get; set; }

        //Events
        public event EventHandler GameStarted;
        public event EventHandler GamePaused;
        public event EventHandler GameUnpaused;
        public event EventHandler GameEnded;
        public event EventHandler GameReseted;

        public event EventHandler GameMustRedraw;

        //Constructor
        public GameEngine(IConfiguration configuration, ITimerService timer, IBoardHolder boardHolder, IBoardFactory boardFactory,
            IFigureFactory figureFactory, IFigureHolder figureHolder, IGameDefeatChecker gameDefeatChecker, ILineCleaner lineCleaner,
            IGameStatistics gameStatistics, IAchievmentsDataCache achievmentsCache, IGameTimeCalculator timeCalculator, 
            IFigureStoreManager figureStore)
        {
            //Services
            this.Configuration = configuration;
            this.BoardHolder = boardHolder;
            this.BoardFactory = boardFactory;
            this.Timer = timer;
            this.FigureFactory = figureFactory;
            this.FigureHolder = figureHolder;
            this.GameDefeatChecker = gameDefeatChecker;
            this.GameStatistics = gameStatistics;
            this.AchievmentsCache = achievmentsCache;
            this.GameTimeCalculator = timeCalculator;
            this.FigureStore = figureStore;

            //Initialize
            this.Reset();
        }

        //Methods
        public void InitailizeBoard()
        {
            int boardWidth = this.Configuration.BoardWidthInBlocks;
            int boardHeight = this.Configuration.BoardHeightInBlocks;
            this.BoardHolder.Board = this.BoardFactory.Create(boardWidth, boardHeight);
        }

        public void InitailizeFigure()
        {
            this.FigureHolder.FallingFigure = this.FigureFactory.Create();
        }

        public void StartGame()
        {
            this.Active = true;
            if (this.FigureHolder.FallingFigure == null)
            {
                this.InitailizeFigure();
            }
            this.Timer.Start();
            this.GameTimeCalculator.Start();

            this.AchievmentsCache.Statistics.GamesCount++;
            this.AchievmentsCache.SaveAchievmentsData();

            this.GameStarted?.Invoke(this, EventArgs.Empty);
        }

        public void EndGame()
        {
            this.Active = false;
            this.IsPaused = false;
            this.Timer.Stop();
            this.GameTimeCalculator.Stop();
            this.AchievmentsCache.SaveAchievmentsData();

            this.GameEnded?.Invoke(this, EventArgs.Empty);
        }

        public void Pause()
        {
            this.Active = false;
            this.IsPaused = true;
            this.Timer.Stop();
            this.GameTimeCalculator.Stop();

            this.GamePaused?.Invoke(this, EventArgs.Empty);
        }

        public void Continue()
        {
            this.Active = true;
            this.IsPaused = false;
            this.Timer.Start();
            this.GameTimeCalculator.Start();

            this.GameUnpaused?.Invoke(this, EventArgs.Empty);
        }

        public void Reset()
        {
            this.AchievmentsCache.SaveAchievmentsData();

            this.Active = false;
            this.IsPaused = false;

            this.Timer.Stop();
            this.GameTimeCalculator.Reset();
            this.InitailizeBoard();
            this.GameStatistics.Reset();
            this.GameDefeatChecker.Reset();
            this.FigureHolder.Reset();
            this.FigureStore.Reset();
            this.RedrawGame();

            this.GameReseted?.Invoke(this, EventArgs.Empty);
        }

        public void RedrawGame()
        {
            this.GameMustRedraw?.Invoke(this, EventArgs.Empty);
        }
    }
}
