﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Tetris.Model;
using Tetris.Services.Contract.Logic;

namespace Tetris.Services.Implementation.Logic
{
    public class FigureRotation : IFigureRotation
    {
        //Services
        public IFigureMoving FigureMoving { get; set; }
        public IFigureMovingCollisions FigureMovingCollisions { get; set; }
        public IFigureHolder FigureHolder { get; set; }
        public IFigureChanger FigureChanger { get; set; }

        //Events
        public event EventHandler<Figure> RotatedLeft;
        public event EventHandler<Figure> RotatedRight;

        //Constructor
        public FigureRotation(IFigureMoving figureMoving, IFigureMovingCollisions collisions, IFigureHolder figureHolder, IFigureChanger figureChanger)
        {
            //Services
            this.FigureMoving = figureMoving;
            this.FigureMovingCollisions = collisions;
            this.FigureHolder = figureHolder;
            this.FigureChanger = figureChanger;
        }

        //Methods
        public void RotateLeft()
        {
            this.FigureHolder.FallingFigure = this.RotateLeft(this.FigureHolder.FallingFigure);
        }

        public Figure RotateLeft(Figure figure)
        {
            this.SetRotation(figure, figure.Angle - 90);
            this.RotatedLeft?.Invoke(this, figure);
            return figure;
        }

        public void RotateRight()
        {
            this.FigureHolder.FallingFigure = this.RotateRight(this.FigureHolder.FallingFigure);
        }

        public Figure RotateRight(Figure figure)
        {
            this.SetRotation(figure, figure.Angle + 90);
            this.RotatedRight?.Invoke(this, figure);
            return figure;
        }

        public Figure SetRotation(Figure figure, int angle)
        {
            figure.Angle = angle;
            figure = this.FigureChanger.CalculateBlocksRelativePositions(figure);

            if (figure != null)
            {
                while (this.FigureMovingCollisions.IsOutsideTheBoardFromLeft(figure))
                {
                    this.FigureMoving.MoveRight(figure);
                }

                while (this.FigureMovingCollisions.IsOutsideTheBoardFromRight(figure))
                {
                    this.FigureMoving.MoveLeft(figure);
                }
            }

            return figure;
        }
    }
}
