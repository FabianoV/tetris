﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Tetris.Model;
using Tetris.Services.Contract.Drawing;
using Tetris.Services.Contract.Logic;

namespace Tetris.Services.Implementation.Drawing
{
    public class GameDrawingBitmap : IGameDrawingBitmap
    {
        //Services
        public IConfiguration Configuration { get; set; }
        public IBoardHolder BoardHolder { get; set; }
        public IFigureHolder FigureHolder { get; set; }
        public IDrawingRenderer Renderer { get; set; }

        //Properties
        public int BlockSize { get; set; }
        public BitmapImage Frame { get; set; }
        public RenderTargetBitmap Target { get; set; }
        public DrawingVisual Visual { get; set; }

        //Constructor
        public GameDrawingBitmap(IConfiguration configuration, IBoardHolder boardHolder, IFigureHolder figureHolder, IDrawingRenderer renderer)
        {
            //Services
            this.Configuration = configuration;
            this.BoardHolder = boardHolder;
            this.Renderer = renderer;
            this.FigureHolder = figureHolder;

            //Initialize
            this.BlockSize = 25;
        }

        //Methods
        public void Redraw()
        {
            int boardWidth = Configuration.BoardWidthInBlocks * this.BlockSize;
            int boardHeight = Configuration.BoardHeightInBlocks * this.BlockSize;

            this.Target = new RenderTargetBitmap(boardWidth, boardHeight, 96, 96, PixelFormats.Pbgra32);
            this.Visual = new DrawingVisual();

            using (DrawingContext drawingContext = this.Visual.RenderOpen())
            {
                this.DrawBoard(drawingContext);
                this.DrawFigure(drawingContext);
            }

            this.Target.Render(this.Visual);
            this.Frame = this.Renderer.RenderTargetBitmapToBitmapImage(this.Target);
        }

        private void CleanBitmap(DrawingContext context)
        {
            int boardWidth = Configuration.BoardWidthInBlocks * this.BlockSize;
            int boardHeight = Configuration.BoardHeightInBlocks * this.BlockSize;
            Rect area = new Rect(0, 0, boardWidth, boardHeight);
            context.DrawRectangle(Brushes.Transparent, null, area);
        }

        private void DrawBoard(DrawingContext context)
        {
            if (this.BoardHolder.Board == null)
            {
                return;
            }

            for (int x = 0; x < BoardHolder.Board.Width; x++)
            {
                for (int y = 0; y < BoardHolder.Board.Height; y++)
                {
                    Block current = this.BoardHolder.GetBlock(x, y);

                    if (current != null)
                    {
                        int posX = x * this.BlockSize;
                        int posY = y * this.BlockSize;
                        Rect area = new Rect(posX, posY, this.BlockSize, this.BlockSize);
                        context.DrawImage(current.Texture, area);
                    }
                }
            }
        }

        private void DrawFigure(DrawingContext context)
        {
            Figure figure = this.FigureHolder.FallingFigure;

            if (figure != null)
            {
                foreach (Block block in figure.Blocks)
                {
                    int posX = (figure.X + block.X) * this.BlockSize;
                    int posY = (figure.Y + block.Y) * this.BlockSize;
                    Rect location = new Rect(posX, posY, this.BlockSize, this.BlockSize);
                    context.DrawImage(block.Texture, location);
                }
            }
        }


    }
}
