﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Tetris.Model;
using Tetris.Model.Figures;
using Tetris.Services.Contract;
using Tetris.Services.Contract.Drawing;
using Tetris.Services.Contract.Logic;

namespace Tetris.Services.Implementation.Drawing
{
    public class NextFiveFiguresPanelDrawing : INextFiveFiguresPanelDrawing
    {
        //Services
        public IConfiguration Configuration { get; set; }
        public IBoardHolder BoardHolder { get; set; }
        public IFigureHolder FigureHolder { get; set; }
        public IDrawingRenderer Renderer { get; set; }
        public IFigureSizeCalculator FigureSizeCalculator { get; set; }

        //Properties
        public int BlockSize { get; set; }
        public int Margin { get; set; }
        public int Border { get; set; }
        public bool IsBorderDrawing { get; set; }
        public int SpaceBettweenFigures { get; set; }
        public int FiguresCount { get; set; }
        public int NextFigureCellPanelWidth { get; set; }
        public int NextFigureCellPanelHeight { get; set; }
        public int NextFigurePanelWidth { get; set; }
        public int NextFigurePanelHeight { get; set; }
        public int FigureCountToDraw { get; set; }
        public BitmapImage Frame { get; set; }
        public RenderTargetBitmap Target { get; set; }
        public DrawingVisual Visual { get; set; }

        //Constructor
        public NextFiveFiguresPanelDrawing(IConfiguration configuration, IBoardHolder boardHolder, IFigureHolder figureHolder,
            IDrawingRenderer renderer, IFigureSizeCalculator figureSizeCalculator)
        {
            //Services
            this.Configuration = configuration;
            this.BoardHolder = boardHolder;
            this.FigureHolder = figureHolder;
            this.Renderer = renderer;
            this.FigureSizeCalculator = figureSizeCalculator;

            //Initialize
            this.FiguresCount = 5;
            this.BlockSize = 25;
            this.Margin = 0;
            this.Border = 1;
            this.FigureCountToDraw = 5;
            this.SpaceBettweenFigures = 3;
            this.NextFigureCellPanelWidth = this.Margin + 4 * this.BlockSize + this.Margin;
            this.NextFigureCellPanelHeight = this.Margin + 4 * this.BlockSize + this.Margin + this.SpaceBettweenFigures;
            this.NextFigurePanelWidth = this.NextFigureCellPanelWidth;
            this.NextFigurePanelHeight = this.NextFigureCellPanelHeight * this.FigureCountToDraw + this.SpaceBettweenFigures * this.FiguresCount;
        }

        //Methods
        public void Redraw()
        {
            this.Target = new RenderTargetBitmap(this.NextFigurePanelWidth, this.NextFigurePanelHeight, 96, 96, PixelFormats.Pbgra32);
            this.Visual = new DrawingVisual();

            using (DrawingContext drawingContext = this.Visual.RenderOpen())
            {
                this.PaintBitmapBlack(drawingContext);
                this.DrawFigures(drawingContext);
                this.DrawBorder(drawingContext);
            }

            this.Target.Render(this.Visual);
            this.Frame = this.Renderer.RenderTargetBitmapToBitmapImage(this.Target);
        }

        private void PaintBitmapBlack(DrawingContext context)
        {
            Rect area = new Rect(0, 0, this.NextFigureCellPanelWidth, this.NextFigureCellPanelHeight);
            context.DrawRectangle(Brushes.Transparent, null, area);
        }

        private void DrawBorder(DrawingContext context)
        {
            if (this.IsBorderDrawing)
            {
                Brush borderColor = Brushes.Red;
                context.DrawRectangle(borderColor, null, new Rect(0, 0, this.Border, this.NextFigureCellPanelHeight * this.FigureCountToDraw)); //Left
                context.DrawRectangle(borderColor, null, new Rect(this.NextFigureCellPanelWidth - this.Border, 0, this.Border, this.NextFigureCellPanelHeight * this.FigureCountToDraw)); //Right
                context.DrawRectangle(borderColor, null, new Rect(0, 0, this.NextFigureCellPanelWidth, this.Border)); //Top
                context.DrawRectangle(borderColor, null, new Rect(0, this.NextFigureCellPanelHeight * this.FigureCountToDraw - this.Border, this.NextFigureCellPanelWidth, this.Border)); //Bottom
            }
        }

        private void DrawFigures(DrawingContext context)
        {
            List<Figure> figures = this.FigureHolder.NextFigures.ToList();
            Point currentPosition = new Point();

            foreach (Figure figure in figures)
            {
                this.DrawFigure(context, figure, currentPosition);
                currentPosition = new Point(0, currentPosition.Y + this.NextFigureCellPanelHeight + this.SpaceBettweenFigures);
            }
        }

        private void DrawFigure(DrawingContext context, Figure figure, Point start)
        {
            if (this.FigureHolder.NextFigures.Count == 0)
            {
                return;
            }

            int figureWidthInBlocks = this.FigureSizeCalculator.CalculateWidthInBlocks(figure);
            int figureHeightInBlocks = this.FigureSizeCalculator.CalculateHeightInBlocks(figure);
            int offsetX = (int)start.X + ((4 - figureWidthInBlocks) * this.BlockSize) / 2;
            int offsetY = (int)start.Y + ((4 - figureHeightInBlocks) * this.BlockSize) / 2;

            //Exception - I figure is not draw from 0;0 position
            if (figure is I)
            {
                offsetX -= 2 * this.BlockSize;
            }

            if (figure != null)
            {
                foreach (Block block in figure.Blocks)
                {
                    int posX = offsetX + block.X * this.BlockSize;
                    int posY = offsetY + block.Y * this.BlockSize;
                    Rect location = new Rect(posX, posY, this.BlockSize, this.BlockSize);
                    context.DrawImage(block.Texture, location);
                }
            }
        }
    }
}
