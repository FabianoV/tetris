﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using Tetris.Services.Contract.Achievments;

namespace Tetris.Services.Implementation.Achievments
{
    public class AchievmentsTextures : IAchievmentsTextures
    {
        //Properties
        public BitmapImage Background { get; set; }
        public BitmapImage FlagIcon { get; set; }
        public BitmapImage NatureIcon { get; set; }
        public BitmapImage LikeIcon { get; set; }
        public BitmapImage BookIcon { get; set; }
        public BitmapImage SymbolIcon { get; set; }
        public BitmapImage ShapesIcon { get; set; }
        public BitmapImage TetrisShapeIcon { get; set; }
        public BitmapImage StrongIcon { get; set; }
        public BitmapImage AtmIcon { get; set; }
        public BitmapImage CachMachineIcon { get; set; }
        public BitmapImage GodIcon { get; set; }
        public BitmapImage MapIcon { get; set; }
        public BitmapImage MechanicsIcon { get; set; }
        public BitmapImage MountianIcon { get; set; }
        public BitmapImage GroceriesIcon { get; set; }
        public BitmapImage BlackboardIcon { get; set; }
        public BitmapImage RisingIcon { get; set; }
        public BitmapImage SofaIcon { get; set; }
        public BitmapImage PinwhellIcon { get; set; }
        public BitmapImage SportCarIcon { get; set; }

        public BitmapImage BronzeStarIcon { get; set; }
        public BitmapImage SilverStarIcon { get; set; }
        public BitmapImage GoldStarIcon { get; set; }
        public BitmapImage RedStarIcon { get; set; }
        public BitmapImage DiamondStarIcon { get; set; }

        public BitmapImage CoinIcon { get; set; }
        public BitmapImage TwoCoinsIcon { get; set; }
        public BitmapImage ManyCoinsIcon { get; set; }
        public BitmapImage MoneyIcon { get; set; }
        public BitmapImage SafeIcon { get; set; }
        public BitmapImage PiggybankIcon { get; set; }

        public bool IsTexturesLoaded { get; set; }
        public string StartupPath { get; set; }

        string achievmentsIconDir = @"Content\Achievments\";

        string emptyBlockTexturePath = @"Achievment_background_purple.png"; 
        string flagIconTexturePath = @"Flag_start_icon.png";
        string natureIconTexturePath = @"001-nature.png";
        string likeIconTexturePath = @"002-like.png";
        string bookIconTexturePath = @"003-book.png";
        string symbolIconTexturePath = @"004-symbol.png";
        string shapesIconTexturePath = @"005-shapes-1.png";
        string tetrisShapeIconTexturePath = @"tetris_shape.png";
        string strongIconTexturePath = @"strong.png";
        string AtmIconTexturePath = @"atm.png";
        string CachMachineIconTexturePath = @"cash-machine.png";
        string GodIconTexturePath = @"god.png";
        string MapIconTexturePath = @"map.png";
        string MechanicsIconTexturePath = @"mechanics.png";
        string MountianIconTexturePath = @"mountian.png";
        string GroceriesIconTexturePath = @"groceries.png";
        string blackboardIconTexturePath = @"blackboard.png";
        string risingIconTexturePath = @"rising.png";
        string bronzeStarIconTexturePath = @"bronze_star.png";
        string silverStarIconTexturePath = @"silver_star.png";
        string goldStarIconTexturePath = @"gold_star.png";
        string redStarIconTexturePath = @"red_star.png";
        string diamondStarIconTexturePath = @"diamond_star.png";
        string sofaIconTexturePath = @"sofa.png";
        string pinwheelIconTexturePath = @"pinwheel.png";
        string sportCarIconTexturePath = @"sport_car.png";

        string coinIconTexturePath = @"coin.png";
        string twocoinsIconTexturePath = @"two_coins.png";
        string manyCoinsIconTexturePath = @"many_coins.png";
        string moneyIconTexturePath = @"money.png";
        string safeIconTexturePath = @"safe.png";
        string piggybankIconTexturePath = @"piggybank.png";

        //Constructor
        public AchievmentsTextures()
        {
            this.IsTexturesLoaded = false;
            this.StartupPath = System.AppDomain.CurrentDomain.BaseDirectory;
            this.LoadTextures();
        }

        //Methods
        public void LoadTextures()
        {
            this.Background = new BitmapImage(new Uri(Path.Combine(StartupPath, achievmentsIconDir, emptyBlockTexturePath)));
            this.FlagIcon = new BitmapImage(new Uri(Path.Combine(StartupPath, achievmentsIconDir, flagIconTexturePath)));
            this.NatureIcon = new BitmapImage(new Uri(Path.Combine(StartupPath, achievmentsIconDir, natureIconTexturePath)));
            this.LikeIcon = new BitmapImage(new Uri(Path.Combine(StartupPath, achievmentsIconDir, likeIconTexturePath)));
            this.BookIcon = new BitmapImage(new Uri(Path.Combine(StartupPath, achievmentsIconDir, bookIconTexturePath)));
            this.SymbolIcon = new BitmapImage(new Uri(Path.Combine(StartupPath, achievmentsIconDir, symbolIconTexturePath)));
            this.ShapesIcon = new BitmapImage(new Uri(Path.Combine(StartupPath, achievmentsIconDir, shapesIconTexturePath)));
            this.TetrisShapeIcon = new BitmapImage(new Uri(Path.Combine(StartupPath, achievmentsIconDir, tetrisShapeIconTexturePath)));
            this.StrongIcon = new BitmapImage(new Uri(Path.Combine(StartupPath, achievmentsIconDir, strongIconTexturePath)));
            this.AtmIcon = new BitmapImage(new Uri(Path.Combine(StartupPath, achievmentsIconDir, AtmIconTexturePath)));
            this.CachMachineIcon = new BitmapImage(new Uri(Path.Combine(StartupPath, achievmentsIconDir, CachMachineIconTexturePath)));
            this.GodIcon = new BitmapImage(new Uri(Path.Combine(StartupPath, achievmentsIconDir, GodIconTexturePath)));
            this.MapIcon = new BitmapImage(new Uri(Path.Combine(StartupPath, achievmentsIconDir, MapIconTexturePath)));
            this.MechanicsIcon = new BitmapImage(new Uri(Path.Combine(StartupPath, achievmentsIconDir, MechanicsIconTexturePath)));
            this.MountianIcon = new BitmapImage(new Uri(Path.Combine(StartupPath, achievmentsIconDir, MountianIconTexturePath)));
            this.GroceriesIcon = new BitmapImage(new Uri(Path.Combine(StartupPath, achievmentsIconDir, GroceriesIconTexturePath)));
            this.BlackboardIcon = new BitmapImage(new Uri(Path.Combine(StartupPath, achievmentsIconDir, blackboardIconTexturePath)));
            this.RisingIcon = new BitmapImage(new Uri(Path.Combine(StartupPath, achievmentsIconDir, risingIconTexturePath)));
            this.PinwhellIcon = new BitmapImage(new Uri(Path.Combine(StartupPath, achievmentsIconDir, pinwheelIconTexturePath)));
            this.SportCarIcon = new BitmapImage(new Uri(Path.Combine(StartupPath, achievmentsIconDir, sportCarIconTexturePath)));

            this.BronzeStarIcon = new BitmapImage(new Uri(Path.Combine(StartupPath, achievmentsIconDir, bronzeStarIconTexturePath)));
            this.SilverStarIcon = new BitmapImage(new Uri(Path.Combine(StartupPath, achievmentsIconDir, silverStarIconTexturePath)));
            this.GoldStarIcon = new BitmapImage(new Uri(Path.Combine(StartupPath, achievmentsIconDir, goldStarIconTexturePath)));
            this.RedStarIcon = new BitmapImage(new Uri(Path.Combine(StartupPath, achievmentsIconDir, redStarIconTexturePath)));
            this.DiamondStarIcon = new BitmapImage(new Uri(Path.Combine(StartupPath, achievmentsIconDir, diamondStarIconTexturePath)));
            this.SofaIcon = new BitmapImage(new Uri(Path.Combine(StartupPath, achievmentsIconDir, sofaIconTexturePath)));

            this.CoinIcon = new BitmapImage(new Uri(Path.Combine(StartupPath, achievmentsIconDir, coinIconTexturePath)));
            this.TwoCoinsIcon = new BitmapImage(new Uri(Path.Combine(StartupPath, achievmentsIconDir, twocoinsIconTexturePath)));
            this.ManyCoinsIcon = new BitmapImage(new Uri(Path.Combine(StartupPath, achievmentsIconDir, manyCoinsIconTexturePath)));
            this.MoneyIcon = new BitmapImage(new Uri(Path.Combine(StartupPath, achievmentsIconDir, moneyIconTexturePath)));
            this.SafeIcon = new BitmapImage(new Uri(Path.Combine(StartupPath, achievmentsIconDir, safeIconTexturePath)));
            this.PiggybankIcon = new BitmapImage(new Uri(Path.Combine(StartupPath, achievmentsIconDir, piggybankIconTexturePath)));

            this.IsTexturesLoaded = true;
        }
    }
}
