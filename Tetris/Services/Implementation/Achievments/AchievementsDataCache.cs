﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Model.Tetris;
using Tetris.Services.Contract.Achievments;

namespace Tetris.Services.Implementation.Achievments
{
    public class AchievementsDataCache : IAchievmentsDataCache
    {
        //Properties
        public Statistics Statistics { get; set; }
        public string FileName { get; set; }

        //Constructor
        public AchievementsDataCache()
        {
            //Initialize
            this.Statistics = new Statistics();
            this.FileName = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "Stats.json");
            this.LoadAchievmentsData();
        }

        //Methods
        public void LoadAchievmentsData()
        {
            if (!File.Exists(this.FileName))
            {
                this.SaveAchievmentsData();
            }

            string json = File.ReadAllText(this.FileName);
            this.Statistics = JsonConvert.DeserializeObject<Statistics>(json);
        }

        public void SaveAchievmentsData()
        {
            string json = JsonConvert.SerializeObject(this.Statistics);
            File.WriteAllText(this.FileName, json);
        }

        public void ResetAchievmentsData()
        {
            this.Statistics = new Statistics()
            {
                CleanedLines = 0,
                Points = 0,

                SingleLine = 0,
                DoubleLine = 0,
                TripleLine = 0,
                Tetris = 0,

                AchievmentHistory = new Dictionary<string, bool>(),
            };

            this.SaveAchievmentsData();
        }
    }
}
