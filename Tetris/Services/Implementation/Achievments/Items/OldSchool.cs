﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Services.Contract.Achievments;

namespace Tetris.Services.Implementation.Achievments.Items
{
    public class OldSchool : BaseAchievment, IAchievment
    {
        //Constructor
        public OldSchool(IAchievmentsTextures textures, IAchievmentsDataCache achievmentCache) 
            : base(textures, achievmentCache)
        {
            //Initialize
            this.Id = 21;
            this.Title = "Old School";
            this.Body = "Clear 300 line in one game.";
            this.Icon = this.Textures.BlackboardIcon;
        }

        //Methods
        protected override bool Predicate()
        {
            return this.CurrentStats.CleanedLines >= 300;
        }
    }
}
