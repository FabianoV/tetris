﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Services.Contract.Achievments;

namespace Tetris.Services.Implementation.Achievments.Items
{
    public class Tetrisomania : BaseAchievment, IAchievment
    {
        //Constructor
        public Tetrisomania(IAchievmentsTextures textures, IAchievmentsDataCache achievmentCache) 
            : base(textures, achievmentCache)
        {
            //Initialize
            this.Id = 24;
            this.Title = "Tetrismania";
            this.Body = "Clear 50 tetris in one game.";
            this.Icon = this.Textures.ShapesIcon;
        }

        //Methods
        protected override bool Predicate()
        {
            return this.CurrentStats.Tetris >= 50;
        }
    }
}
