﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Services.Contract.Achievments;

namespace Tetris.Services.Implementation.Achievments.Items
{
    public class Advanced : BaseAchievment, IAchievment
    {
        //Constructor
        public Advanced(IAchievmentsTextures textures, IAchievmentsDataCache achievmentCache) 
            : base(textures, achievmentCache)
        {
            //Initialize
            this.Id = 2;
            this.Title = "Advanced";
            this.Body = "You are cleared 500 lines in total.";
            this.Icon = this.Textures.GoldStarIcon;
        }

        //Methods
        protected override bool Predicate()
        {
            return this.Stats.CleanedLines >= 500;
        }
    }
}
