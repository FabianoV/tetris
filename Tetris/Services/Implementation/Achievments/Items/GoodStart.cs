﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Tetris.Services.Contract.Achievments;

namespace Tetris.Services.Implementation.Achievments.Items
{
    public class GoodStart : BaseAchievment, IAchievment
    {
        //Constructor
        public GoodStart(IAchievmentsTextures textures, IAchievmentsDataCache achievmentCache) 
            : base(textures, achievmentCache)
        {
            //Initialize
            this.Id = 10;
            this.Title = "Good Start";
            this.Body = "Clear 25 lines in one game.";
            this.Icon = this.Textures.FlagIcon;
        }

        //Methods
        protected override bool Predicate()
        {
            return this.CurrentStats.CleanedLines > 25;
        }
    }
}
