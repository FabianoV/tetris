﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Services.Contract.Achievments;

namespace Tetris.Services.Implementation.Achievments.Items
{
    public class NiceGame : BaseAchievment, IAchievment
    {
        //Constructor
        public NiceGame(IAchievmentsTextures textures, IAchievmentsDataCache achievmentCache) 
            : base(textures, achievmentCache)
        {
            //Initialize
            this.Id = 18;
            this.Title = "Nice Game";
            this.Body = "Clear 50 line in one game.";
            this.Icon = this.Textures.AtmIcon;
        }

        //Methods
        protected override bool Predicate()
        {
            return this.CurrentStats.CleanedLines >= 50;
        }
    }
}
