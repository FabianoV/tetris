﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Services.Contract.Achievments;

namespace Tetris.Services.Implementation.Achievments.Items
{
    public class _100Lines : BaseAchievment, IAchievment
    {
        //Constructor
        public _100Lines(IAchievmentsTextures textures, IAchievmentsDataCache achievmentCache) 
            : base(textures, achievmentCache)
        {
            //Initialize
            this.Id = 11;
            this.Title = "100 Lines.";
            this.Body = "You are cleared 100 lines in one game.";
            this.Icon = this.Textures.StrongIcon;
        }

        //Methods
        protected override bool Predicate()
        {
            return this.CurrentStats.CleanedLines >= 100;
        }
    }
}
