﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Services.Contract.Achievments;

namespace Tetris.Services.Implementation.Achievments.Items
{
    public class Tetris : BaseAchievment, IAchievment
    {
        //Constructor
        public Tetris(IAchievmentsTextures textures, IAchievmentsDataCache achievmentCache) 
            : base(textures, achievmentCache)
        {
            //Initialize
            this.Id = 22;
            this.Title = "Tetris!";
            this.Body = "Clear 4 lines at once.";
            this.Icon = this.Textures.LikeIcon;
        }

        //Methods
        protected override bool Predicate()
        {
            return this.Stats.Tetris > 0;
        }
    }
}
