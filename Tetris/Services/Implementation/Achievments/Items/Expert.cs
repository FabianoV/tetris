﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Services.Contract.Achievments;

namespace Tetris.Services.Implementation.Achievments.Items
{
    public class Expert : BaseAchievment, IAchievment
    {
        //Constructor
        public Expert(IAchievmentsTextures textures, IAchievmentsDataCache achievmentCache) 
            : base(textures, achievmentCache)
        {
            //Initialize
            this.Id = 3;
            this.Title = "Expert";
            this.Body = "You have cleared 3000 lines in total.";
            this.Icon = this.Textures.RedStarIcon;
        }

        //Methods
        protected override bool Predicate()
        {
            return this.Stats.CleanedLines >= 3000;
        }
    }
}
