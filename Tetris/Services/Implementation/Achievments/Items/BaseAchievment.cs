﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using Tetris.Model.Tetris;
using Tetris.Services.Contract.Achievments;

namespace Tetris.Services.Implementation.Achievments.Items
{
    public abstract class BaseAchievment : IAchievment
    {
        //Services
        public IAchievmentsTextures Textures { get; set; }
        public IAchievmentsDataCache AchievmentCache { get; set; }

        //Properties
        public int Id { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public bool IsReached
        {
            get
            {
                if (IsReachedFakeEnable)
                {
                    return this.IsReachedFakeValue;
                }
                return this.IsAchivmentReachedInHistory();
            }
        }
        public bool IsReachedFakeEnable { get; set; }
        public bool IsReachedFakeValue { get; set; }
        public BitmapImage Background { get; set; }
        public BitmapImage Icon { get; set; }

        public CurrentGameStatistics CurrentStats { get; set; }
        public Statistics Stats { get { return this.AchievmentCache.Statistics; } }

        //Constructor
        public BaseAchievment(IAchievmentsTextures textures, IAchievmentsDataCache achievmentCache)
        {
            //Services
            this.Textures = textures;
            this.AchievmentCache = achievmentCache;

            //Initialize
            this.Id = int.MaxValue;
            this.Background = this.Textures.Background;
            this.IsReachedFakeEnable = false;
        }

        //Methods
        protected abstract bool Predicate();
        private void SetSingleGameAchievmentStatus(bool value)
        {
            if (this.Stats.AchievmentHistory.ContainsKey(this.Title))
            {
                this.Stats.AchievmentHistory[this.Title] = value;
            }
            else
            {
                this.Stats.AchievmentHistory.Add(this.Title, value);
            }
        }
        private bool IsAchivmentReachedInHistory()
        {
            if (!this.Stats.AchievmentHistory.ContainsKey(this.Title))
            {
                return false;
            }

            return this.Stats.AchievmentHistory[this.Title];
        }
        protected virtual void ValidateStats()
        {
            if (this.CurrentStats == null)
            {
                throw new Exception("CurrentStats is null");
            }
        }
        public bool CheckIsReached()
        {
            this.ValidateStats();

            if (this.IsAchivmentReachedInHistory())
            {
                return true;
            }
            else
            {
                bool result = this.Predicate();
                if (result == true)
                {
                    this.SetSingleGameAchievmentStatus(true);
                    this.AchievmentCache.SaveAchievmentsData();
                }
                return result;
            }
        }
        public void FakeIsReached(bool value)
        {
            this.IsReachedFakeValue = value;
            this.IsReachedFakeEnable = true;
        }
        public void DisableFake()
        {
            this.IsReachedFakeEnable = false;
        }
    }
}
