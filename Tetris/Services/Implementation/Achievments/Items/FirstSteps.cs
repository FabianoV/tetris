﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using Tetris.Services.Contract.Achievments;

namespace Tetris.Services.Implementation.Achievments.Items
{
    public class FirstSteps : BaseAchievment, IAchievment
    {
        //Constructor
        public FirstSteps(IAchievmentsTextures textures, IAchievmentsDataCache achievmentCache) 
            : base(textures, achievmentCache)
        {
            //Initialize
            this.Id = 0;
            this.Title = "First Steps";
            this.Body = "You are cleared 1 line.";
            this.Icon = this.Textures.BronzeStarIcon;
        }

        //Methods
        protected override bool Predicate()
        {
            return this.Stats.CleanedLines >= 1;
        }
    }
}
