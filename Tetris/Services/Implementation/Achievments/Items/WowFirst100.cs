﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Services.Contract.Achievments;

namespace Tetris.Services.Implementation.Achievments.Items
{
    public class WowFirst100 : BaseAchievment, IAchievment
    {
        //Constructor
        public WowFirst100(IAchievmentsTextures textures, IAchievmentsDataCache achievmentCache) 
            : base(textures, achievmentCache)
        {
            //Initialize
            this.Id = 19;
            this.Title = "Wow, first 100!";
            this.Body = "Clear 100 line in one game.";
            this.Icon = this.Textures.MapIcon;
        }

        //Methods
        protected override bool Predicate()
        {
            return this.CurrentStats.CleanedLines >= 100;
        }
    }
}
