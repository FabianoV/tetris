﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Services.Contract.Achievments;

namespace Tetris.Services.Implementation.Achievments.Items
{
    public class YouGetSomePoints : BaseAchievment, IAchievment
    {
        //Constructor
        public YouGetSomePoints(IAchievmentsTextures textures, IAchievmentsDataCache achievmentCache) 
            : base(textures, achievmentCache)
        {
            //Initialize
            this.Id = 5;
            this.Title = "You get some points";
            this.Body = "Get 10000 points in total.";
            this.Icon = this.Textures.CoinIcon;
        }

        //Methods
        protected override bool Predicate()
        {
            return this.Stats.Points >= 10000;
        }
    }
}
