﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Services.Contract.Achievments;

namespace Tetris.Services.Implementation.Achievments.Items
{
    public class FasterAndFaster : BaseAchievment, IAchievment
    {
        //Constructor
        public FasterAndFaster(IAchievmentsTextures textures, IAchievmentsDataCache achievmentCache) 
            : base(textures, achievmentCache)
        {
            //Initialize
            this.Id = 17;
            this.Title = "Faster and Faster.";
            this.Body = "You get 5000 points in three minutes.";
            this.Icon = this.Textures.SportCarIcon;
        }

        //Methods
        protected override bool Predicate()
        {
            return this.CurrentStats.CleanedLines >= 100;
        }
    }
}
