﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Services.Contract.Achievments;

namespace Tetris.Services.Implementation.Achievments.Items
{
    public class CasualPlayer : BaseAchievment, IAchievment
    {
        //Constructor
        public CasualPlayer(IAchievmentsTextures textures, IAchievmentsDataCache achievmentCache) 
            : base(textures, achievmentCache)
        {
            //Initialize
            this.Id = 13;
            this.Title = "Casual Player";
            this.Body = "Play 10 games.";
            this.Icon = this.Textures.TetrisShapeIcon;
        }

        //Methods
        protected override bool Predicate()
        {
            return this.Stats.GamesCount >= 10;
        }
    }
}
