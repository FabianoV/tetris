﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Services.Contract.Achievments;

namespace Tetris.Services.Implementation.Achievments.Items
{
    public class GoodDirection : BaseAchievment, IAchievment
    {
        //Constructor
        public GoodDirection(IAchievmentsTextures textures, IAchievmentsDataCache achievmentCache) 
            : base(textures, achievmentCache)
        {
            //Initialize
            this.Id = 16;
            this.Title = "Good Direction";
            this.Body = "Get 5000 points in first five minutes.";
            this.Icon = this.Textures.PinwhellIcon;
        }

        //Methods
        protected override bool Predicate()
        {
            return this.CurrentStats.Points > 1000 && (DateTime.Now - this.CurrentStats.GameStart).Seconds < 60 * 5;
        }
    }
}
