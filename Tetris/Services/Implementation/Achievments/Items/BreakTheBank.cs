﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Services.Contract.Achievments;

namespace Tetris.Services.Implementation.Achievments.Items
{
    public class BreakTheBank : BaseAchievment, IAchievment
    {
        //Constructor
        public BreakTheBank(IAchievmentsTextures textures, IAchievmentsDataCache achievmentCache) 
            : base(textures, achievmentCache)
        {
            //Initialize
            this.Id = 9;
            this.Title = "Break the bank.";
            this.Body = "You get 1000000 points in total.";
            this.Icon = this.Textures.SafeIcon;
        }

        //Methods
        protected override bool Predicate()
        {
            return this.Stats.CleanedLines >= 1000000;
        }
    }
}
