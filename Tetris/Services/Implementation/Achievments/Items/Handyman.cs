﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Services.Contract.Achievments;

namespace Tetris.Services.Implementation.Achievments.Items
{
    public class Handyman : BaseAchievment, IAchievment
    {
        //Constructor
        public Handyman(IAchievmentsTextures textures, IAchievmentsDataCache achievmentCache) 
            : base(textures, achievmentCache)
        {
            //Initialize
            this.Id = 15;
            this.Title = "Handyman";
            this.Body = "Get 1000 points in first minute.";
            this.Icon = this.Textures.MechanicsIcon;
        }

        //Methods
        protected override bool Predicate()
        {
            return this.CurrentStats.Points > 1000 && (DateTime.Now - this.CurrentStats.GameStart).Seconds < 60;
        }
    }
}
