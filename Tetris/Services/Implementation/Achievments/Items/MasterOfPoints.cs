﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Services.Contract.Achievments;

namespace Tetris.Services.Implementation.Achievments.Items
{
    public class MasterOfPoints : BaseAchievment, IAchievment
    {
        //Constructor
        public MasterOfPoints(IAchievmentsTextures textures, IAchievmentsDataCache achievmentCache) 
            : base(textures, achievmentCache)
        {
            //Initialize
            this.Id = 8;
            this.Title = "Master of Points";
            this.Body = "You get 500000 points in total.";
            this.Icon = this.Textures.ManyCoinsIcon;
        }

        //Methods
        protected override bool Predicate()
        {
            return this.Stats.Points >= 500000;
        }
    }
}
