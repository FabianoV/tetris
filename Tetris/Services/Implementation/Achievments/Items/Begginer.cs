﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Services.Contract.Achievments;

namespace Tetris.Services.Implementation.Achievments.Items
{
    public class Begginer : BaseAchievment, IAchievment
    {
        //Constructor
        public Begginer(IAchievmentsTextures textures, IAchievmentsDataCache achievmentCache) 
            : base(textures, achievmentCache)
        {
            //Initialize
            this.Id = 1;
            this.Title = "Begginer";
            this.Body = "You are cleared 100 line in total.";
            this.Icon = this.Textures.SilverStarIcon;
        }

        //Methods
        protected override bool Predicate()
        {
            return this.Stats.CleanedLines >= 100;
        }
    }
}
