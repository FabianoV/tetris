﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Services.Contract.Achievments;

namespace Tetris.Services.Implementation.Achievments.Items
{
    public class PermamentPlayer : BaseAchievment, IAchievment
    {
        //Constructor
        public PermamentPlayer(IAchievmentsTextures textures, IAchievmentsDataCache achievmentCache)
            : base(textures, achievmentCache)
        {
            //Initialize
            this.Id = 14;
            this.Title = "Permament Player";
            this.Body = "Play 100 games.";
            this.Icon = this.Textures.SofaIcon;
        }

        //Methods
        protected override bool Predicate()
        {
            return this.Stats.GamesCount >= 100;
        }
    }
}
