﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Services.Contract.Achievments;

namespace Tetris.Services.Implementation.Achievments.Items
{
    public class Lucky : BaseAchievment, IAchievment
    {
        public Random Random { get; set; }

        //Constructor
        public Lucky(IAchievmentsTextures textures, IAchievmentsDataCache achievmentCache) 
            : base(textures, achievmentCache)
        {
            //Initialize
            this.Random = new Random();
            this.Id = 25;
            this.Title = "Lucky.";
            this.Body = "You have a chance to win the game..";
            this.Icon = this.Textures.NatureIcon;
        }

        //Methods
        protected override bool Predicate()
        {
            return this.Random.NextDouble() <= 0.001;
        }
    }
}
