﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Services.Contract.Achievments;

namespace Tetris.Services.Implementation.Achievments.Items
{
    public class Master : BaseAchievment, IAchievment
    {
        //Constructor
        public Master(IAchievmentsTextures textures, IAchievmentsDataCache achievmentCache) 
            : base(textures, achievmentCache)
        {
            //Initialize
            this.Id = 4;
            this.Title = "Master";
            this.Body = "You are cleared 10000 lines in total.";
            this.Icon = this.Textures.DiamondStarIcon;
        }

        //Methods
        protected override bool Predicate()
        {
            return this.Stats.CleanedLines >= 10000;
        }
    }
}
