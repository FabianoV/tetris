﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Services.Contract.Achievments;

namespace Tetris.Services.Implementation.Achievments.Items
{
    public class GoldOportunity : BaseAchievment, IAchievment
    {
        //Constructor
        public GoldOportunity(IAchievmentsTextures textures, IAchievmentsDataCache achievmentCache) 
            : base(textures, achievmentCache)
        {
            //Initialize
            this.Id = 20;
            this.Title = "Gold Oportunity";
            this.Body = "Clear 200 line in one game.";
            this.Icon = this.Textures.MountianIcon;
        }

        //Methods
        protected override bool Predicate()
        {
            return this.CurrentStats.CleanedLines >= 200;
        }
    }
}
