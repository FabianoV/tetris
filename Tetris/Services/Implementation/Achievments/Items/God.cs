﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Services.Contract.Achievments;

namespace Tetris.Services.Implementation.Achievments.Items
{
    public class God : BaseAchievment, IAchievment
    {
        //Constructor
        public God(IAchievmentsTextures textures, IAchievmentsDataCache achievmentCache) 
            : base(textures, achievmentCache)
        {
            //Initialize
            this.Id = 12;
            this.Title = "God.";
            this.Body = "You are cleared 300 lines in one game.";
            this.Icon = this.Textures.GodIcon;
        }

        //Methods
        protected override bool Predicate()
        {
            return this.CurrentStats.CleanedLines >= 300;
        }
    }
}
