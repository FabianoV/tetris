﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Services.Contract.Achievments;

namespace Tetris.Services.Implementation.Achievments.Items
{
    public class PerfectTetris : BaseAchievment, IAchievment
    {
        //Constructor
        public PerfectTetris(IAchievmentsTextures textures, IAchievmentsDataCache achievmentCache) 
            : base(textures, achievmentCache)
        {
            //Initialize
            this.Id = 23;
            this.Title = "Perfect Tetris";
            this.Body = "Clear 15 tetris in one game.";
            this.Icon = this.Textures.RisingIcon;
        }

        //Methods
        protected override bool Predicate()
        {
            return this.CurrentStats.Tetris >= 15;
        }
    }
}
