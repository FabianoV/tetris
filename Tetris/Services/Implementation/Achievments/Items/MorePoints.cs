﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Services.Contract.Achievments;

namespace Tetris.Services.Implementation.Achievments.Items
{
    public class MorePoints : BaseAchievment, IAchievment
    {
        //Constructor
        public MorePoints(IAchievmentsTextures textures, IAchievmentsDataCache achievmentCache) 
            : base(textures, achievmentCache)
        {
            //Initialize
            this.Id = 6;
            this.Title = "More Points.";
            this.Body = "You get 25000 points in total.";
            this.Icon = this.Textures.TwoCoinsIcon;
        }

        //Methods
        protected override bool Predicate()
        {
            return this.Stats.Points >= 25000;
        }
    }
}
