﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Services.Contract.Achievments;

namespace Tetris.Services.Implementation.Achievments.Items
{
    public class ManyMorePoints : BaseAchievment, IAchievment
    {
        //Constructor
        public ManyMorePoints(IAchievmentsTextures textures, IAchievmentsDataCache achievmentCache) 
            : base(textures, achievmentCache)
        {
            //Initialize
            this.Id = 7;
            this.Title = "Many More Points";
            this.Body = "You get 100000 points in total.";
            this.Icon = this.Textures.MoneyIcon;
        }

        //Methods
        protected override bool Predicate()
        {
            return this.Stats.Points >= 100000;
        }
    }
}
