﻿using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using Tetris.Model.Achievments;
using Tetris.Model.Tetris;
using Tetris.Services.Contract.Achievments;
using Tetris.Services.Implementation.Achievments.Items;

namespace Tetris.Services.Implementation.Achievments
{
    public class AchievementsDataHolder : IAchievementsDataHolder
    {
        //Services
        public IAchievmentsTextures Textures { get; set; }
        public IAchievmentsDataCache AchievmentsCache { get; set; }

        //Proeprties
        public List<IAchievment> Achievments { get; set; }
        public List<IAchievment> AchievmentsLastReached { get; set; }

        //Events
        public event EventHandler<EventArgs> AchivmentsReached;

        //Constructor
        public AchievementsDataHolder(IAchievmentsTextures textures, IAchievmentsDataCache achievmentsCache, IEnumerable<IAchievment> baseAchievments)
        {
            //Services
            this.Textures = textures;
            this.AchievmentsCache = achievmentsCache;

            //Initialize
            this.Achievments = baseAchievments.OrderBy(a => a.Id).ToList();
            this.CalulateIsAchievmentsReached(CurrentGameStatistics.Empty);
        }

        //Methods
        public void CalulateIsAchievmentsReached(CurrentGameStatistics currentStats)
        {
            foreach (IAchievment achievment in this.Achievments)
            {
                achievment.CurrentStats = currentStats;
                achievment.CheckIsReached();
            }
        }

        public IEnumerable<IAchievment> CheckAchievmentsNowReached(CurrentGameStatistics currentStats)
        {
            List<IAchievment> notReached = this.Achievments.Where(a => !a.IsReached).ToList();
            this.CalulateIsAchievmentsReached(currentStats);
            this.AchievmentsLastReached = notReached.Where(a => a.IsReached).ToList();
            if (this.AchievmentsLastReached.Count > 0)
            {
                this.AchivmentsReached?.Invoke(this, EventArgs.Empty);
            }

            return this.AchievmentsLastReached;
        }
    }
}
