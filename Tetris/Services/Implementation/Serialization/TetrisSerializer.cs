﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Tetris.Model;
using Tetris.Model.Tetris.Serialization;
using Tetris.Services.Contract.Logic;
using Tetris.Services.Contract.Serialization;

namespace Tetris.Services.Implementation.Serialization
{
    public class TetrisSerializer : ITetrisSerializer
    {
        //Services
        public IBoardHolder BoardHolder { get; set; }
        public IFigureHolder FigureHolder { get; set; }
        public IBlockToSerializationBlockConverter BlockConverter { get; set; }
        public IFigureToSerializationFigureConverter FigureConverter { get; set; }
        public IGameStatistics Statistics { get; set; }

        //Constructor
        public TetrisSerializer(IBoardHolder boardHolder, IFigureHolder figureHolder, IGameStatistics statistics,
            IBlockToSerializationBlockConverter blockConverter, IFigureToSerializationFigureConverter figureConverter)
        {
            //Services
            this.BoardHolder = boardHolder;
            this.FigureHolder = figureHolder;
            this.BlockConverter = blockConverter;
            this.FigureConverter = figureConverter;
            this.Statistics = statistics;
        }

        //Methods
        public SerializableTetrisGameState Serialize()
        {
            SerializableTetrisGameState gameState = new SerializableTetrisGameState()
            {
                Metadata = new SerializationMetadata()
                {
                    SerializationTime = DateTime.Now,
                    SerializationName = "",
                    Version = Assembly.GetEntryAssembly().GetName().Version,
                },
                Board = new SerializableBoard()
                {
                    Blocks = new List<SerializableBlock>(),
                    Height = this.BoardHolder.Board.Height,
                    Width = this.BoardHolder.Board.Width,
                    FallingFigure = this.FigureConverter.Convert(this.FigureHolder.FallingFigure),
                    StoredFigure = this.FigureConverter.Convert(this.FigureHolder.StoredFigure),
                    NextFigures = new List<SerializableFigure>(),
                },
                Statistics = new SerializableStatistics()
                {
                    GameStart = this.Statistics.Statistics.GameStart,
                    CleanedLines = this.Statistics.Statistics.CleanedLines,
                    Points = this.Statistics.Statistics.Points,
                    SingleLine = this.Statistics.Statistics.SingleLine,
                    DoubleLine = this.Statistics.Statistics.DoubleLine,
                    TripleLine = this.Statistics.Statistics.TripleLine,
                    Tetris = this.Statistics.Statistics.Tetris,
                },
            };

            //Blockss
            foreach (Block block in this.BoardHolder.Board.Blocks)
            {
                SerializableBlock sBlock = this.BlockConverter.Convert(block);
                gameState.Board.Blocks.Add(sBlock);
            }

            //Next Figures
            foreach (Figure figure in this.FigureHolder.NextFigures)
            {
                SerializableFigure sFigure = this.FigureConverter.Convert(figure);
                gameState.Board.NextFigures.Add(sFigure);
            }

            return gameState;
        }
    }
}
