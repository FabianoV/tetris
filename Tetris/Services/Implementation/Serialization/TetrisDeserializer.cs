﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Model;
using Tetris.Model.Tetris.Serialization;
using Tetris.Services.Contract.Logic;
using Tetris.Services.Contract.Serialization;

namespace Tetris.Services.Implementation.Serialization
{
    public class TetrisDeserializer : ITetrisDeserializer
    {
        //Properties
        public IBoardHolder BoardHolder { get; set; }
        public IFigureHolder FigureHolder { get; set; }
        public IFigureFactory FigureFactory { get; set; }
        public IBlockToSerializationBlockConverter BlockConverter { get; set; }
        public IFigureToSerializationFigureConverter FigureConverter { get; set; }
        public IGameStatistics Statistics { get; set; }

        //Constructor
        public TetrisDeserializer(IBoardHolder boardHolder, IFigureHolder figureHolder, IFigureFactory figureFactory, IGameStatistics statistics,
            IBlockToSerializationBlockConverter blockConverter, IFigureToSerializationFigureConverter figureConverter)
        {
            //Services
            this.BoardHolder = boardHolder;
            this.FigureHolder = figureHolder;
            this.FigureFactory = figureFactory;
            this.BlockConverter = blockConverter;
            this.FigureConverter = figureConverter;
            this.Statistics = statistics;
        }

        //Methods
        public void Deserialize(SerializableTetrisGameState gameState)
        {
            //Board
            Board board = new Board()
            {
                Blocks = new List<Block>(),
                Width = gameState.Board.Width,
                Height = gameState.Board.Height,
            };
            foreach (SerializableBlock sBlock in gameState.Board.Blocks)
            {
                Block block = this.BlockConverter.ConvertBack(sBlock);
                board.Blocks.Add(block);
            }
            this.BoardHolder.Board = board;

            //Falling Figure
            Figure fallingFigure = this.FigureConverter.ConvertBack(gameState.Board.FallingFigure);
            this.FigureHolder.FallingFigure = fallingFigure;

            //Stored Figure
            Figure storedFigure = this.FigureConverter.ConvertBack(gameState.Board.StoredFigure);
            this.FigureHolder.StoredFigure = storedFigure;

            //Next Figures
            List<Figure> nextFigures = new List<Figure>();
            foreach (SerializableFigure sNextFigure in gameState.Board.NextFigures)
            {
                Figure nextFigure = this.FigureConverter.ConvertBack(sNextFigure);
                nextFigures.Add(nextFigure);
            }
            this.FigureHolder.NextFigures = new Queue<Figure>(nextFigures);

            //Statistics
            this.Statistics.Statistics = new Model.Tetris.CurrentGameStatistics()
            {
                GameStart = gameState.Statistics.GameStart,
                CleanedLines = gameState.Statistics.CleanedLines,
                Points = gameState.Statistics.Points,
                SingleLine = gameState.Statistics.SingleLine,
                DoubleLine = gameState.Statistics.DoubleLine,
                TripleLine = gameState.Statistics.TripleLine,
                Tetris = gameState.Statistics.Tetris,
            };
        }
    }
}
