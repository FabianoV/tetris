﻿using Tetris.Model;
using Tetris.Model.Tetris.Serialization;
using Tetris.Services.Contract.Logic;
using Tetris.Services.Contract.Serialization;

namespace Tetris.Services.Implementation.Serialization
{
    public class BlockToSerializationBlockConverter : IBlockToSerializationBlockConverter
    {
        //Services
        public IBlockFactory BlockFactory { get; set; }
        public IBlockTexturesService Textures { get; set; }

        //Constructor
        public BlockToSerializationBlockConverter(IBlockFactory blockFactory, IBlockTexturesService textures)
        {
            //Services
            this.Textures = textures;
            this.BlockFactory = blockFactory;
        }

        //Methods
        public SerializableBlock Convert(Block block)
        {
            SerializableBlock sBlock = new SerializableBlock()
            {
                X = block.X,
                Y = block.Y,
                TextureName = this.Textures.BitmapImageToTextureName(block.Texture),
            };

            return sBlock;
        }

        public Block ConvertBack(SerializableBlock serializableBlock)
        {
            Block block = this.BlockFactory.Create(serializableBlock.TextureName, serializableBlock.X, serializableBlock.Y);
            return block;
        }
    }
}
