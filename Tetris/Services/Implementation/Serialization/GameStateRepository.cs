﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using Tetris.Model.Tetris;
using Tetris.Model.Tetris.Serialization;
using Tetris.Services.Contract.Serialization;

namespace Tetris.Services.Implementation.Serialization
{
    public class GameStateRepository : IGameStateRepository
    {
        //Constants
        public const string DEFAULT_GAME_STATE_DIRECTORY = @"Saves\";
        public const string SAVE_DATA_FORMAT_EXTENSION = ".dat";
        public const string SAVE_IMAGE_DATA_EXTENSION = ".jpg";
        public const string SAVE_DATE_FORMAT = "yyyy-MM-dd_hh-mm-ss-tt";

        //Services
        public ITetrisSerializer Serializer { get; set; }
        public ITetrisDeserializer Deserializer { get; set; }

        //Constructor
        public GameStateRepository(ITetrisSerializer serializer, ITetrisDeserializer deserializer)
        {
            //Services
            this.Serializer = serializer;
            this.Deserializer = deserializer;
        }

        //Methods
        public SaveInfo Save(SaveInfo info)
        {
            SaveInfo result = null;

            try
            {
                result = TrySave(info);
            }
            catch (Exception)
            {
                // TODO: Log
            }

            return result;
        }

        public SaveInfo Load(SaveInfo info)
        {
            SaveInfo result = null;

            try
            {
                result = TryLoad(info);
            }
            catch (Exception)
            {
                // TODO: Log
            }

            return result;
        }

        public void Remove(SaveInfo info)
        {
            try
            {
                TryRemove(info);
            }
            catch (Exception)
            {
                // TODO: Log
            }
        }

        public IEnumerable<SaveInfo> GetSaveList(string path)
        {
            IEnumerable<SaveInfo> list = new List<SaveInfo>();

            try
            {
                list = TryGetSaveList(path);
            }
            catch (Exception)
            {
                // TODO: Log
            }

            return list;
        }

        public SaveInfo TrySave(SaveInfo info)
        {
            //Create save name
            info.Created = DateTime.Now;
            info.Name = SaveInfoToFileName(info);

            //If save directory is not set then get default
            if (info.DirPath == null)
            {
                info.DirPath = DEFAULT_GAME_STATE_DIRECTORY;
            }

            //If save directory is not existsthen create
            string saveDir = Path.GetDirectoryName(info.DirPath);
            if (!Directory.Exists(saveDir))
            {
                Directory.CreateDirectory(saveDir);
            }

            //Create save filenames and paths
            info.DataFileName = Path.Combine(info.DirPath, info.Name) + SAVE_DATA_FORMAT_EXTENSION;
            info.ThumbFileName = Path.Combine(info.DirPath, info.Name) + SAVE_IMAGE_DATA_EXTENSION;

            //Save game state
            info.GameState = this.Serializer.Serialize();
            string json = JsonConvert.SerializeObject(info.GameState);
            File.WriteAllText(info.DataFileName, json);

            //Save thumb image
            BitmapEncoder encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(info.ThumbImage));
            FileStream fileStream = File.OpenWrite(info.ThumbFileName);
            encoder.Save(fileStream);
            fileStream.Close();

            return info;
        }

        public SaveInfo TryLoad(SaveInfo info)
        {
            this.ValidSaveInfo(info);

            //If save directory is not set then get default
            if (info.DirPath == null || string.IsNullOrWhiteSpace(info.DirPath))
            {
                info.DirPath = DEFAULT_GAME_STATE_DIRECTORY;
            }

            //Check if default dir exists
            if (!Directory.Exists(info.DirPath))
            {
                throw new ArgumentException("Save directory not exists");
            }

            //Create save filenames and paths
            info.DataFileName = this.GetDataFilePath(info);
            info.ThumbFileName = this.GetThumbFilePath(info);

            //Load game state
            string json = File.ReadAllText(info.DataFileName);
            info.GameState = JsonConvert.DeserializeObject<SerializableTetrisGameState>(json);

            //Load save thumb image
            info.ThumbImage = new BitmapImage(new Uri(info.ThumbFileName));

            return info;
        }

        public void TryRemove(SaveInfo info)
        {
            this.ValidSaveInfo(info);

            //If save directory is not set then get default
            if (info.DirPath == null || string.IsNullOrWhiteSpace(info.DirPath))
            {
                info.DirPath = DEFAULT_GAME_STATE_DIRECTORY;
            }

            //Create save filenames and paths
            info.DataFileName = this.GetDataFilePath(info);
            info.ThumbFileName = this.GetThumbFilePath(info);

            //Delete data file
            if (File.Exists(info.DataFileName))
            {
                File.Delete(info.DataFileName);
            }

            //Delete thumb file
            if (File.Exists(info.ThumbFileName))
            {
                File.Delete(info.ThumbFileName);
            }
        }

        public IEnumerable<SaveInfo> TryGetSaveList(string path)
        {
            string saveDir = DEFAULT_GAME_STATE_DIRECTORY;
            if (!string.IsNullOrWhiteSpace(path))
            {
                saveDir = Path.GetDirectoryName(path);
            }

            List<SaveInfo> saves = new List<SaveInfo>();
            if (Directory.Exists(saveDir))
            {
                saves = Directory.GetFiles(saveDir)
                    .Where(file => file.EndsWith(SAVE_DATA_FORMAT_EXTENSION))
                    .Select(file => FileNameToSaveInfo(file)).ToList();
            }

            foreach (SaveInfo info in saves)
            {
                //Create save filenames and paths
                info.DirPath = saveDir;
                info.DataFileName = this.GetDataFilePath(info);
                info.ThumbFileName = this.GetThumbFilePath(info);

                //Load save thumb image
                info.ThumbImage = new BitmapImage(new Uri(info.ThumbFileName, UriKind.Absolute));
            }

            return saves;
        }

        private string SaveInfoToFileName(SaveInfo info)
        {
            return string.Format("save {0}", info.Created.ToString(SAVE_DATE_FORMAT));
        }
        private SaveInfo FileNameToSaveInfo(string fileName)
        {
            fileName = fileName.Split('\\', '/').Last();
            fileName = fileName.Replace(SAVE_DATA_FORMAT_EXTENSION, "");
            fileName = fileName.Split('/').Last();
            string[] data = fileName.Split(' ');
            SaveInfo info = new SaveInfo()
            {
                Name = data[0],
                Created = GetDateFromSaveFormat(data[1]),
            };
            return info;
        }
        private void ValidSaveInfo(SaveInfo info)
        {
            if (info.Name == null && info.Name.Contains("."))
            {
                throw new ArgumentException("Is it with file extension? Name can't have character '.'");
            }
        }
        private DateTime GetDateFromSaveFormat(string dateTime)
        {
            return DateTime.ParseExact(dateTime, SAVE_DATE_FORMAT, CultureInfo.InvariantCulture);
        }
        private string GetDataFilePath(SaveInfo info)
        {
            return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, info.DirPath, info.Name + " " + 
                info.Created.ToString(SAVE_DATE_FORMAT)) + SAVE_DATA_FORMAT_EXTENSION;
        }
        private string GetThumbFilePath(SaveInfo info)
        {
            return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, info.DirPath, info.Name + " " + 
                info.Created.ToString(SAVE_DATE_FORMAT)) + SAVE_IMAGE_DATA_EXTENSION;
        }
    }
}
