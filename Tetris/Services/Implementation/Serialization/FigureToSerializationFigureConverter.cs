﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Model;
using Tetris.Model.Figures;
using Tetris.Model.Tetris.Serialization;
using Tetris.Services.Contract.Logic;
using Tetris.Services.Contract.Serialization;

namespace Tetris.Services.Implementation.Serialization
{
    public class FigureToSerializationFigureConverter : IFigureToSerializationFigureConverter
    {
        //Services
        public IBlockToSerializationBlockConverter BlockConverter { get; set; }
        public IFigureFactory FigureFactory { get; set; }
        public IFigureChanger FigureChanger { get; set; }

        //Constructor
        public FigureToSerializationFigureConverter(IBlockToSerializationBlockConverter blockConverter, IFigureFactory figureFactory, 
            IFigureChanger figureChanger)
        {
            //Services
            this.BlockConverter = blockConverter;
            this.FigureFactory = figureFactory;
            this.FigureChanger = figureChanger;
        }

        //Methods
        public SerializableFigure Convert(Figure figure)
        {
            SerializableFigure sFigure = new SerializableFigure()
            {
                Angle = figure.Angle,
                X = figure.X,
                Y = figure.Y,
                Blocks = new List<SerializableBlock>(),
                Type = this.FigureTypeToString(figure),
            };

            foreach (Block block in figure.Blocks)
            {
                SerializableBlock sBlock = this.BlockConverter.Convert(block);
                sFigure.Blocks.Add(sBlock);
            }

            return sFigure;
        }

        public Figure ConvertBack(SerializableFigure serializableFigure)
        {
            Type figureType = this.StringToFigureType(serializableFigure.Type);
            Figure figure = this.FigureFactory.CreateEmpty(figureType);
            figure.X = serializableFigure.X;
            figure.Y = serializableFigure.Y;
            figure.Angle = serializableFigure.Angle;
            figure.Blocks = new List<Block>();
            foreach (SerializableBlock sBlock in serializableFigure.Blocks)
            {
                Block block = this.BlockConverter.ConvertBack(sBlock);
                figure.Blocks.Add(block);
            }
            figure.NumberOfBlocks = serializableFigure.Blocks.Count;
            this.FigureChanger.CalculateBlocksRelativePositions(figure);
            return figure;
        }

        private string FigureTypeToString(Figure figure)
        {
            if (figure is I) return "I";
            if (figure is J) return "J";
            if (figure is L) return "L";
            if (figure is O) return "O";
            if (figure is S) return "S";
            if (figure is T) return "T";
            if (figure is Z) return "Z";

            throw new NotSupportedException("Bad figure type in serialization.");
        }
        private Type StringToFigureType(string type)
        {
            if (type == "I") return typeof(I);
            if (type == "J") return typeof(J);
            if (type == "L") return typeof(L);
            if (type == "O") return typeof(O);
            if (type == "S") return typeof(S);
            if (type == "T") return typeof(T);
            if (type == "Z") return typeof(Z);

            throw new NotSupportedException("Bad figure type in serialization.");
        }
    }
}
