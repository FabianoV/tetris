﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Model.Rank;
using Tetris.Services.Contract.Rank;

namespace Tetris.Services.Implementation.Rank
{
    public class RankHolder : IRankHolder
    {
        //Proeprties
        public List<RankItem> RankItems { get; set; }

        //Constructor
        public RankHolder()
        {
            this.RankItems = new List<RankItem>();
        }

        //Methods
        public void SortByPosition()
        {
            this.RankItems = this.RankItems.OrderByDescending(rankItem => rankItem.Position).ToList();
        }

        public void SortByPoints()
        {
            this.RankItems = this.RankItems.OrderByDescending(rankItem => rankItem.Points).ToList();
        }

        public void AddItem(RankItem item)
        {
            this.RankItems.Add(item);
            this.SortByPoints();
            this.CalculateRankNumeration();
        }

        private void CalculateRankNumeration()
        {
            foreach (int position in Enumerable.Range(1, this.RankItems.Count))
            {
                this.RankItems[position - 1].Position = position;
            }
        }
    }
}
