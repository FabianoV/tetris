﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using Tetris.Model.Rank;
using Tetris.Model.Rank.Results;
using Tetris.Services.Contract.Rank;

namespace Tetris.Services.Implementation.Rank
{
    public class RankApi : IRankApi
    {
        //Services
        public IRankHolder RankHolder { get; set; }

        //Properties
        public string RankFilePath { get; set; } = "rank.json";

        //Constructor
        public RankApi(IRankHolder rankHolder)
        {
            //Services
            this.RankHolder = rankHolder;

            //Initialize
            this.GetRank(limit: 10, offset: 0);
        }

        //Methods
        public GetRankResult GetRank(int offset, int limit)
        {
            GetRankResult result = new GetRankResult();
            string rankJson = null;
            if (File.Exists(RankFilePath))
            {
                rankJson = File.ReadAllText(RankFilePath);
            }
            result.Map(rankJson, HttpStatusCode.OK);
            this.RankHolder.RankItems = result.RankItems;
            return result;
        } 

        public SetRankItemResult SendRankItem(RankItem rank)
        {
            SetRankItemResult result = new SetRankItemResult();
            if (this.RankHolder.RankItems == null || this.RankHolder.RankItems.Count == 0)
            {
                GetRankResult rankSet = this.GetRank(limit: 10, offset: 0);
                this.RankHolder.RankItems = rankSet.RankItems;
            }
            this.RankHolder.AddItem(rank);
            string rankJson = JsonConvert.SerializeObject(this.RankHolder.RankItems);
            File.WriteAllText(RankFilePath, rankJson);
            return result;
        }

        public ResetRankResult ResetRank()
        {
            ResetRankResult result = new ResetRankResult();
            if (File.Exists(RankFilePath))
            {
                File.Delete(RankFilePath);
            }
            this.GetRank(offset: 0, limit: 10);
            return result;
        }
    }
}
