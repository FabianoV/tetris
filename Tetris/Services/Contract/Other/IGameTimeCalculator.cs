﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tetris.Services.Contract.Other
{
    public interface IGameTimeCalculator
    {
        TimeSpan CurrentTime { get; set; }

        event EventHandler NextSecond;

        void Start();
        void Stop();
        void Reset();
    }
}
