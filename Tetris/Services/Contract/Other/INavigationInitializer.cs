﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Tetris.Services.Contract.Other
{
    public interface INavigationInitializer
    {
        void Initialize(Frame frame);
    }
}
