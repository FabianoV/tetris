﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Model.Rank;
using Tetris.Model.Rank.Results;

namespace Tetris.Services.Contract.Rank
{
    public interface IRankApi
    {
        SetRankItemResult SendRankItem(RankItem rank);
        GetRankResult GetRank(int offset, int limit);
        ResetRankResult ResetRank();
    }
}
