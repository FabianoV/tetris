﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Model.Rank;

namespace Tetris.Services.Contract.Rank
{
    public interface IRankHolder
    {
        List<RankItem> RankItems { get; set; }

        void SortByPosition();
        void SortByPoints();
        void AddItem(RankItem item);
    }
}
