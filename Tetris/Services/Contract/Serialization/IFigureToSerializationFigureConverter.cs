﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Model;
using Tetris.Model.Tetris.Serialization;

namespace Tetris.Services.Contract.Serialization
{
    public interface IFigureToSerializationFigureConverter
    {
        SerializableFigure Convert(Figure block);
        Figure ConvertBack(SerializableFigure serializableBlock);
    }
}
