﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Model.Tetris;

namespace Tetris.Services.Contract.Serialization
{
    public interface IGameStateRepository
    {
        SaveInfo Save(SaveInfo gameState);
        SaveInfo Load(SaveInfo saveInfo);
        IEnumerable<SaveInfo> GetSaveList(string saveDir="");
        void Remove(SaveInfo saveInfo);
    }
}
