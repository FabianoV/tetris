﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Model;

namespace Tetris.Services.Contract.Logic
{
    public interface IFigureRotation
    {
        Figure SetRotation(Figure figure, int angle);
        void RotateLeft();
        Figure RotateLeft(Figure figure);
        void RotateRight();
        Figure RotateRight(Figure figure);

        event EventHandler<Figure> RotatedLeft;
        event EventHandler<Figure> RotatedRight;
    }
}
