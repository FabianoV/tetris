﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Model.Tetris;

namespace Tetris.Services.Contract.Logic
{
    public interface ILevelManager
    {
        bool IsLevelChangeEnabled { get; set; }
        LevelInfo CurrentLevel { get; }

        event EventHandler LevelChanged;

        void CheckDifficultyLevel();
        void RaiseDifficultyLevel();
        void LowerDifficultyLevel();
    }
}
