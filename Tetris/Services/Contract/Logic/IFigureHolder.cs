﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using Tetris.Model;

namespace Tetris.Services.Contract.Logic
{
    public interface IFigureHolder
    {
        Figure FallingFigure { get; set; }
        Figure StoredFigure { get; set; }
        Queue<Figure> NextFigures { get; set; }
        List<Type> FiguresTypes { get; set; }
        Dictionary<Type, BitmapImage> FigureTextures { get; set; }

        void Reset();
    }
}
