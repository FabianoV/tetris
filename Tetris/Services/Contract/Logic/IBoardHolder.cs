﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Model;

namespace Tetris.Services.Contract.Logic
{
    public interface IBoardHolder
    {
        Board Board { get; set; }

        Block GetBlock(int x, int y);
    }
}
