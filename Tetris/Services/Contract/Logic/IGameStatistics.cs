﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Model.Tetris;

namespace Tetris.Services.Contract.Logic
{
    public interface IGameStatistics
    {
        CurrentGameStatistics Statistics { get; set; }

        event EventHandler PointCounterChanged;

        void AddPointsForCleanLines(int numberOfLines);
        void Reset();
    }
}
