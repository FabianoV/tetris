﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Tetris.Services.Contract.Logic
{
    public interface ITimerService
    {
        event EventHandler TimerTick;

        void Start();
        void Stop();
        void SetInterval(TimeSpan interval);
    }
}
