﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Model;

namespace Tetris.Services.Contract.Logic
{
    public interface IFigureMoving
    {
        void MoveLeft();
        void MoveLeft(Figure figure);
        void MoveRight();
        void MoveRight(Figure figure);
        void MoveDown();
        void MoveDown(Figure figure);
        void MoveDownToBottom();
        void MoveDownToBottom(Figure figure);

        event EventHandler<Figure> FigureMoveDown;
        event EventHandler<Figure> FigureMoveLeft;
        event EventHandler<Figure> FigureMoveRight;
    }
}
