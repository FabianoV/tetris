﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Model;

namespace Tetris.Services.Contract.Logic
{
    public interface IFigureSizeCalculator
    {
        int CalculateWidthInBlocks(Figure figure);
        int CalculateHeightInBlocks(Figure figure);
    }
}
