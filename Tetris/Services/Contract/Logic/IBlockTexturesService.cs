﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Tetris.Services.Contract.Logic
{
    public interface IBlockTexturesService
    {
        bool IsTexturesLoaded { get; }

        BitmapImage EmptyBlockTexture { get; }
        BitmapImage YellowBlockTexture { get; }
        BitmapImage BlueBlockTexture { get; }
        BitmapImage RedBlockTexture { get; }
        BitmapImage GreenBlockTexture { get; }
        BitmapImage PurpleBlockTexture { get; }
        BitmapImage OrangeBlockTexture { get; }
        BitmapImage DarkBlueBlockTexture { get; }

        void LoadTextures();

        string BitmapImageToTextureName(BitmapImage bitmap);
        BitmapImage TextureNameToBitmapImage(string textureName);
    }
}
