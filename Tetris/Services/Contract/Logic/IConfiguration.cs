﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tetris.Services.Contract.Logic
{
    public interface IConfiguration
    {
        int BoardWidthInBlocks { get; set; }
        int BoardHeightInBlocks { get; set; }

        int FigureStartingPositionX { get; }
        int FigureStartingPositionY { get; }

        int TimerSpeed { get; set; }
    }
}
