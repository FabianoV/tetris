﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tetris.Services.Contract.Logic
{
    public interface IFigureStoreManager
    {
        bool IsFigureStored { get; set; }
        bool CanStoreFigure { get; set; }

        void StoreFallingFigure();
        void Reset();
    }
}
