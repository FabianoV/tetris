﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Model;

namespace Tetris.Services.Contract.Logic
{
    public interface IFigurePutter
    {
        void PutFallingFigureOnBoard();

        event EventHandler<Figure> FigurePuttedOnBoard;
    }
}
