﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Model;

namespace Tetris.Services.Contract.Logic
{
    public interface IFigureMovingCollisions
    {
        bool HasCollisionLeft();
        bool HasCollisionLeft(Figure figure);
        bool HasCollisionRight();
        bool HasCollisionRight(Figure figure);
        bool HasCollisionDown();
        bool HasCollisionDown(Figure figure);

        bool IsOutsideTheBoardFromLeft();
        bool IsOutsideTheBoardFromLeft(Figure figure);
        bool IsOutsideTheBoardFromRight();
        bool IsOutsideTheBoardFromRight(Figure figure);
    }
}
