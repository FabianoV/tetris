﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tetris.Services.Contract.Logic
{
    public interface IGameControlHandlers
    {
        void RotateLeft();
        void RotateRight();

        void MoveDown();
        void MoveLeft();
        void MoveRight();
    }
}
