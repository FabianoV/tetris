﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Tetris.Services.Contract.Logic
{
    public interface IGameEngine
    {
        bool Active { get; }
        bool IsPaused { get; }

        void InitailizeBoard();
        void InitailizeFigure();
        void StartGame();
        void EndGame();
        void RedrawGame();
        void Pause();
        void Continue();
        void Reset();

        event EventHandler GameStarted;
        event EventHandler GamePaused;
        event EventHandler GameUnpaused;
        event EventHandler GameEnded;
        event EventHandler GameReseted;

        event EventHandler GameMustRedraw;
    }
}
