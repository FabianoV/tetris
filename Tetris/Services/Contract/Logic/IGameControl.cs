﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Tetris.Services.Contract.Logic
{
    public interface IGameControl
    {
        void SendKey(Key key);
    }
}
