﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Model;

namespace Tetris.Services.Contract.Logic
{
    public interface IBoardChanger
    {
        void ChangeBlock(Block newBlock, int x, int y);
        void RemoveFromBoard(int x, int y);
        bool IsBlockEmpty(int x, int y);
    }
}
