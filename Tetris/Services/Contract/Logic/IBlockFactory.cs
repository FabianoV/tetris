﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using Tetris.Model;

namespace Tetris.Services.Contract.Logic
{
    public interface IBlockFactory
    {
        Block CreateEmpty(int x, int y);
        Block Create(string textureName, int x, int y);
        Block Create(BitmapImage texture, int x, int y);
    }
}
