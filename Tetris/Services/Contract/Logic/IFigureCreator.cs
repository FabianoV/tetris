﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Model;

namespace Tetris.Services.Contract.Logic
{
    public interface IFigureFactory
    {
        Figure Create();
        Figure Create(Type type);

        Figure CreateEmpty(Type type);

        event EventHandler<Figure> FigureCreated;
    }
}
