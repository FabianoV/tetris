﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using Tetris.Model.Tetris;

namespace Tetris.Services.Contract.Achievments
{
    public interface IAchievment
    {
        int Id { get; set; }
        string Title { get; set; }
        string Body { get; set; }
        bool IsReached { get; }
        BitmapImage Background { get; set; }
        BitmapImage Icon { get; set; }

        CurrentGameStatistics CurrentStats { set; }

        bool CheckIsReached();
        void FakeIsReached(bool value); //Hack for MVVM
        void DisableFake(); //Hack for MVVM
    }
}
