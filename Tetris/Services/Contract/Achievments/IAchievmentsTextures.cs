﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Tetris.Services.Contract.Achievments
{
    public interface IAchievmentsTextures
    {
        bool IsTexturesLoaded { get; }

        BitmapImage Background { get; }

        BitmapImage BronzeStarIcon { get; }
        BitmapImage SilverStarIcon { get; }
        BitmapImage GoldStarIcon { get; }
        BitmapImage RedStarIcon { get; }
        BitmapImage DiamondStarIcon { get; }

        BitmapImage CoinIcon { get; }
        BitmapImage TwoCoinsIcon { get; }
        BitmapImage ManyCoinsIcon { get; }
        BitmapImage MoneyIcon { get; }
        BitmapImage SafeIcon { get; }
        BitmapImage PiggybankIcon { get; }

        BitmapImage FlagIcon { get; }
        BitmapImage NatureIcon { get; }
        BitmapImage LikeIcon { get; }
        BitmapImage BookIcon { get; }
        BitmapImage SymbolIcon { get; }
        BitmapImage ShapesIcon { get; }
        BitmapImage TetrisShapeIcon { get; }
        BitmapImage StrongIcon { get; }
        BitmapImage AtmIcon { get; }
        BitmapImage CachMachineIcon { get; }
        BitmapImage GodIcon { get; }
        BitmapImage MapIcon { get; }
        BitmapImage MechanicsIcon { get; }
        BitmapImage MountianIcon { get; }
        BitmapImage GroceriesIcon { get; }
        BitmapImage BlackboardIcon { get; }
        BitmapImage RisingIcon { get; }
        BitmapImage SofaIcon { get; }
        BitmapImage PinwhellIcon { get; }
        BitmapImage SportCarIcon { get; }

        void LoadTextures();
    }
}
