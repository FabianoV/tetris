﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Model.Achievments;
using Tetris.Model.Tetris;

namespace Tetris.Services.Contract.Achievments
{
    public interface IAchievementsDataHolder
    {
        List<IAchievment> Achievments { get; }
        List<IAchievment> AchievmentsLastReached { get; }
        event EventHandler<EventArgs> AchivmentsReached;
        IEnumerable<IAchievment> CheckAchievmentsNowReached(CurrentGameStatistics currentStats);
    }
}
