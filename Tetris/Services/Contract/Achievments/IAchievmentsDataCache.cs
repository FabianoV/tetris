﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Model.Tetris;

namespace Tetris.Services.Contract.Achievments
{
    public interface IAchievmentsDataCache
    {
        string FileName { get; }
        Statistics Statistics { get; set; }

        void SaveAchievmentsData();
        void LoadAchievmentsData();
        void ResetAchievmentsData();
    }
}
