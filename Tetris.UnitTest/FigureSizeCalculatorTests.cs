﻿using GalaSoft.MvvmLight.Ioc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Model;
using Tetris.Model.Figures;
using Tetris.Services.Contract.Logic;

namespace Tetris.UnitTest
{
    [TestClass]
    public class FigureSizeCalculatorTests : BaseTests
    {
        [TestMethod]
        public void FigureSizeCalculator_RegistredInIoc_NotNull()
        {
            //Arrange
            IFigureSizeCalculator calculator = SimpleIoc.Default.GetInstance<IFigureSizeCalculator>();

            //Assert
            Assert.IsNotNull(calculator);
        }

        [TestMethod]
        public void FigureSizeCalculator_CalculateWidthForOFigure_HasCorrectResult()
        {
            //Arrange
            IFigureSizeCalculator calculator = SimpleIoc.Default.GetInstance<IFigureSizeCalculator>();
            IFigureFactory factory = SimpleIoc.Default.GetInstance<IFigureFactory>();
            Figure figure = factory.Create(typeof(O));

            //Act
            int width = calculator.CalculateWidthInBlocks(figure);

            //Assert
            Assert.AreEqual(2, width);
        }

        [TestMethod]
        public void FigureSizeCalculator_CalculateHeightForOFigure_HasCorrectResult()
        {
            //Arrange
            IFigureSizeCalculator calculator = SimpleIoc.Default.GetInstance<IFigureSizeCalculator>();
            IFigureFactory factory = SimpleIoc.Default.GetInstance<IFigureFactory>();
            Figure figure = factory.Create(typeof(O));

            //Act
            int height = calculator.CalculateHeightInBlocks(figure);

            //Assert
            Assert.AreEqual(2, height);
        }

        [TestMethod]
        public void FigureSizeCalculator_CalculateWidthForIFigure_HasCorrectResult()
        {
            //Arrange
            IFigureSizeCalculator calculator = SimpleIoc.Default.GetInstance<IFigureSizeCalculator>();
            IFigureFactory factory = SimpleIoc.Default.GetInstance<IFigureFactory>();
            Figure figure = factory.Create(typeof(I));

            //Act
            int width = calculator.CalculateWidthInBlocks(figure);

            //Assert
            Assert.AreEqual(1, width);
        }

        [TestMethod]
        public void FigureSizeCalculator_CalculateHeightForIFigure_HasCorrectResult()
        {
            //Arrange
            IFigureSizeCalculator calculator = SimpleIoc.Default.GetInstance<IFigureSizeCalculator>();
            IFigureFactory factory = SimpleIoc.Default.GetInstance<IFigureFactory>();
            Figure figure = factory.Create(typeof(I));

            //Act
            int height = calculator.CalculateHeightInBlocks(figure);

            //Assert
            Assert.AreEqual(4, height);
        }

        [TestMethod]
        public void FigureSizeCalculator_CalculateWidthForJFigure_HasCorrectResult()
        {
            //Arrange
            IFigureSizeCalculator calculator = SimpleIoc.Default.GetInstance<IFigureSizeCalculator>();
            IFigureFactory factory = SimpleIoc.Default.GetInstance<IFigureFactory>();
            Figure figure = factory.Create(typeof(J));

            //Act
            int width = calculator.CalculateWidthInBlocks(figure);

            //Assert
            Assert.AreEqual(2, width);
        }

        [TestMethod]
        public void FigureSizeCalculator_CalculateHeightForJFigure_HasCorrectResult()
        {
            //Arrange
            IFigureSizeCalculator calculator = SimpleIoc.Default.GetInstance<IFigureSizeCalculator>();
            IFigureFactory factory = SimpleIoc.Default.GetInstance<IFigureFactory>();
            Figure figure = factory.Create(typeof(J));

            //Act
            int height = calculator.CalculateHeightInBlocks(figure);

            //Assert
            Assert.AreEqual(3, height);
        }

        [TestMethod]
        public void FigureSizeCalculator_CalculateWidthForLFigure_HasCorrectResult()
        {
            //Arrange
            IFigureSizeCalculator calculator = SimpleIoc.Default.GetInstance<IFigureSizeCalculator>();
            IFigureFactory factory = SimpleIoc.Default.GetInstance<IFigureFactory>();
            Figure figure = factory.Create(typeof(L));

            //Act
            int width = calculator.CalculateWidthInBlocks(figure);

            //Assert
            Assert.AreEqual(2, width);
        }

        [TestMethod]
        public void FigureSizeCalculator_CalculateHeightForLFigure_HasCorrectResult()
        {
            //Arrange
            IFigureSizeCalculator calculator = SimpleIoc.Default.GetInstance<IFigureSizeCalculator>();
            IFigureFactory factory = SimpleIoc.Default.GetInstance<IFigureFactory>();
            Figure figure = factory.Create(typeof(L));

            //Act
            int height = calculator.CalculateHeightInBlocks(figure);

            //Assert
            Assert.AreEqual(3, height);
        }

        [TestMethod]
        public void FigureSizeCalculator_CalculateWidthForSFigure_HasCorrectResult()
        {
            //Arrange
            IFigureSizeCalculator calculator = SimpleIoc.Default.GetInstance<IFigureSizeCalculator>();
            IFigureFactory factory = SimpleIoc.Default.GetInstance<IFigureFactory>();
            Figure figure = factory.Create(typeof(S));

            //Act
            int width = calculator.CalculateWidthInBlocks(figure);

            //Assert
            Assert.AreEqual(3, width);
        }

        [TestMethod]
        public void FigureSizeCalculator_CalculateHeightForSFigure_HasCorrectResult()
        {
            //Arrange
            IFigureSizeCalculator calculator = SimpleIoc.Default.GetInstance<IFigureSizeCalculator>();
            IFigureFactory factory = SimpleIoc.Default.GetInstance<IFigureFactory>();
            Figure figure = factory.Create(typeof(S));

            //Act
            int height = calculator.CalculateHeightInBlocks(figure);

            //Assert
            Assert.AreEqual(2, height);
        }

        [TestMethod]
        public void FigureSizeCalculator_CalculateWidthForTFigure_HasCorrectResult()
        {
            //Arrange
            IFigureSizeCalculator calculator = SimpleIoc.Default.GetInstance<IFigureSizeCalculator>();
            IFigureFactory factory = SimpleIoc.Default.GetInstance<IFigureFactory>();
            Figure figure = factory.Create(typeof(T));

            //Act
            int width = calculator.CalculateWidthInBlocks(figure);

            //Assert
            Assert.AreEqual(3, width);
        }

        [TestMethod]
        public void FigureSizeCalculator_CalculateHeightForTFigure_HasCorrectResult()
        {
            //Arrange
            IFigureSizeCalculator calculator = SimpleIoc.Default.GetInstance<IFigureSizeCalculator>();
            IFigureFactory factory = SimpleIoc.Default.GetInstance<IFigureFactory>();
            Figure figure = factory.Create(typeof(T));

            //Act
            int height = calculator.CalculateHeightInBlocks(figure);

            //Assert
            Assert.AreEqual(2, height);
        }

        [TestMethod]
        public void FigureSizeCalculator_CalculateWidthForZFigure_HasCorrectResult()
        {
            //Arrange
            IFigureSizeCalculator calculator = SimpleIoc.Default.GetInstance<IFigureSizeCalculator>();
            IFigureFactory factory = SimpleIoc.Default.GetInstance<IFigureFactory>();
            Figure figure = factory.Create(typeof(Z));

            //Act
            int width = calculator.CalculateWidthInBlocks(figure);

            //Assert
            Assert.AreEqual(3, width);
        }

        [TestMethod]
        public void FigureSizeCalculator_CalculateHeightForZFigure_HasCorrectResult()
        {
            //Arrange
            IFigureSizeCalculator calculator = SimpleIoc.Default.GetInstance<IFigureSizeCalculator>();
            IFigureFactory factory = SimpleIoc.Default.GetInstance<IFigureFactory>();
            Figure figure = factory.Create(typeof(Z));

            //Act
            int height = calculator.CalculateHeightInBlocks(figure);

            //Assert
            Assert.AreEqual(2, height);
        }
    }
}
