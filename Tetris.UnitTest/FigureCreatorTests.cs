﻿using GalaSoft.MvvmLight.Ioc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Model;
using Tetris.Model.Figures;
using Tetris.Services.Contract.Logic;

namespace Tetris.UnitTest
{
    [TestClass]
    public class FigureFactoryTests : BaseTests
    {
        [TestMethod]
        public void FigureFactory_CreateRandomFigure_IsNotNull()
        {
            //Arrange
            IFigureFactory figureFactory = SimpleIoc.Default.GetInstance<IFigureFactory>();

            //Act
            Figure figure = figureFactory.Create();

            //Assert
            Assert.IsNotNull(figure);
        }

        [TestMethod]
        public void FigureFactory_CreateFigureI_IsNotNull()
        {
            //Arrange
            IFigureFactory figureFactory = SimpleIoc.Default.GetInstance<IFigureFactory>();

            //Act
            Figure figure = figureFactory.Create(typeof(I));

            //Assert
            Assert.IsNotNull(figure);
        }

        [TestMethod]
        public void FigureFactory_CreateFigureI_IsTypeOfIFigure()
        {
            //Arrange
            IFigureFactory figureFactory = SimpleIoc.Default.GetInstance<IFigureFactory>();

            //Act
            Figure figure = figureFactory.Create(typeof(I));

            //Assert
            Assert.IsInstanceOfType(figure, typeof(I));
        }

        [TestMethod]
        public void FigureFactory_CreateFigureI_TextureIsNotNull()
        {
            //Arrange
            IFigureFactory figureFactory = SimpleIoc.Default.GetInstance<IFigureFactory>();

            //Act
            Figure figure = figureFactory.Create(typeof(I));

            //Assert
            Assert.IsNotNull(figure.Texture);
        }

        [TestMethod]
        public void FigureFactory_CreateFigureI_BlocksListIsNotNull()
        {
            //Arrange
            IFigureFactory figureFactory = SimpleIoc.Default.GetInstance<IFigureFactory>();

            //Act
            Figure figure = figureFactory.Create(typeof(I));

            //Assert
            Assert.IsNotNull(figure.Blocks);
        }

        [TestMethod]
        public void FigureFactory_CreateFigureI_BlocksListHasMoreThanOneBlock()
        {
            //Arrange
            IFigureFactory figureFactory = SimpleIoc.Default.GetInstance<IFigureFactory>();

            //Act
            Figure figure = figureFactory.Create(typeof(I));

            //Assert
            Assert.IsTrue(figure.Blocks.Count > 1);
        }
    }
}
