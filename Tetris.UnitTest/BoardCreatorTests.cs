﻿using GalaSoft.MvvmLight.Ioc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Model;
using Tetris.Services.Contract.Logic;

namespace Tetris.UnitTest
{
    [TestClass]
    public class BoardFactoryTests
    {
        [TestMethod]
        public void BoardFactory_Create_ResultIsNotNull()
        {
            //Arrange
            IBoardFactory boardFactory = SimpleIoc.Default.GetInstance<IBoardFactory>();
            int width = 10;
            int height = 8;

            //Act
            Board board = boardFactory.Create(width, height);

            //Assert
            Assert.IsNotNull(board);
        }
    }
}
