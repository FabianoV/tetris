﻿using GalaSoft.MvvmLight.Ioc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tetris.Services.Contract.Logic;

namespace Tetris.UnitTest
{
    [TestClass]
    public class GameEngineSeriveceTests
    {
        [TestMethod]
        public void GameEngine_InitializeBoard_BoardIsNotNull()
        {
            //Arrange
            IGameEngine gameEngine = SimpleIoc.Default.GetInstance<IGameEngine>();
            IBoardHolder boardHolder = SimpleIoc.Default.GetInstance<IBoardHolder>();

            //Act
            gameEngine.InitailizeBoard();

            //Assert
            Assert.IsNotNull(boardHolder.Board);
        }

        [TestMethod]
        public void GameEngine_InitializeFigure_FigureIsNotNull()
        {
            //Arrange
            IGameEngine gameEngine = SimpleIoc.Default.GetInstance<IGameEngine>();
            IFigureHolder boardHolder = SimpleIoc.Default.GetInstance<IFigureHolder>();

            //Act
            gameEngine.InitailizeFigure();

            //Assert
            Assert.IsNotNull(boardHolder.FallingFigure);
        }

        [TestMethod]
        public void GameEngine_BeforeStart_IsGameNotActive()
        {
            //Arrange
            IGameEngine gameEngine = SimpleIoc.Default.GetInstance<IGameEngine>();

            //Assert
            Assert.IsFalse(gameEngine.Active);
        }

        [TestMethod]
        public void GameEngine_AfterStart_IsGameActive()
        {
            //Arrange
            IGameEngine gameEngine = SimpleIoc.Default.GetInstance<IGameEngine>();

            //Act
            gameEngine.StartGame();

            //Assert
            Assert.IsTrue(gameEngine.Active);
        }
    }
}
