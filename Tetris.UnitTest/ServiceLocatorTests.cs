﻿using GalaSoft.MvvmLight.Ioc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Services.Contract;
using Tetris.Services.Contract.Logic;
using Tetris.ViewModel;

namespace Tetris.UnitTest
{ 
    [TestClass] 
    public class ServiceLocatorTests : BaseTests
    {
        //[TestMethod]
        //public void ServiceLocator_IsBoardViewModel_IsNotNull()
        //{
            //Act
            //BoardVewModel service = SimpleIoc.Default.GetInstance<BoardVewModel>();

            //Assert
            //Assert.IsNotNull(service);
        //}

        [TestMethod]
        public void ServiceLocator_IsGameSettingsViewModel_IsNotNull()
        {
            //Act
            GameSettingsViewModel service = SimpleIoc.Default.GetInstance<GameSettingsViewModel>();

            //Assert
            Assert.IsNotNull(service);
        }

        [TestMethod]
        public void ServiceLocator_IsBlockTextureService_IsNotNull()
        {
            //Act
            IBlockTexturesService service = SimpleIoc.Default.GetInstance<IBlockTexturesService>();

            //Assert
            Assert.IsNotNull(service);
        }

        [TestMethod]
        public void ServiceLocator_IsBoardFactory_IsNotNull()
        {
            //Act
            IBoardFactory service = SimpleIoc.Default.GetInstance<IBoardFactory>();

            //Assert
            Assert.IsNotNull(service);
        }

        [TestMethod]
        public void ServiceLocator_IsBlockFactory_IsNotNull()
        {
            //Act
            IBlockFactory service = SimpleIoc.Default.GetInstance<IBlockFactory>();

            //Assert
            Assert.IsNotNull(service);
        }

    }
}
