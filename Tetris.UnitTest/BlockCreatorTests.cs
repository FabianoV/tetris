﻿using GalaSoft.MvvmLight.Ioc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Model;
using Tetris.Services.Contract.Logic;

namespace Tetris.UnitTest
{
    [TestClass]
    public class BlockFactoryTests : BaseTests
    {
        [TestMethod]
        public void BlockFactory_CreateEmptyBlock_ReturnsEmptyBlock()
        {
            //Arrange
            IBlockFactory blockFactory = SimpleIoc.Default.GetInstance<IBlockFactory>();
            IBlockTexturesService textureBlockService = SimpleIoc.Default.GetInstance<IBlockTexturesService>();
            int x = 5;
            int y = 7;

            //Act
            Block block = blockFactory.CreateEmpty(x, y);

            //Assert
            Assert.IsNotNull(block);
            Assert.AreEqual(textureBlockService.EmptyBlockTexture, block.Texture);
        }

        [TestMethod]
        public void BlockFactory_CreateRedBlock_ReturnsBlock()
        {
            //Arrange
            IBlockFactory blockFactory = SimpleIoc.Default.GetInstance<IBlockFactory>();
            IBlockTexturesService textureBlockService = SimpleIoc.Default.GetInstance<IBlockTexturesService>();
            int x = 5;
            int y = 7;
            string blockType = "red";

            //Act
            Block redBlock = blockFactory.Create(blockType, x, y);

            //Assert
            Assert.IsNotNull(redBlock);
            Assert.AreEqual(textureBlockService.RedBlockTexture, redBlock.Texture);
        }

        [TestMethod]
        [ExpectedException(typeof(NotImplementedException))]
        public void BlockFactory_CreateNotExistingBlock_ReturnsBlock()
        {
            //Arrange
            IBlockFactory blockFactory = SimpleIoc.Default.GetInstance<IBlockFactory>();
            IBlockTexturesService textureBlockService = SimpleIoc.Default.GetInstance<IBlockTexturesService>();
            int x = 5;
            int y = 7;
            string blockType = "something";

            //Act
            Block redBlock = blockFactory.Create(blockType, x, y);
        }
    }
}
