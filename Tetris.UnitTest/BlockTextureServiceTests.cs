﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using GalaSoft.MvvmLight.Ioc;
using Tetris.Services.Contract.Logic;

namespace Tetris.UnitTest
{
    [TestClass]
    public class BlockTextureServiceTests : BaseTests
    {
        [TestMethod]
        public void BlockTexture_Textures_Loaded()
        {
            //Arrange
            IBlockTexturesService blocktextures = SimpleIoc.Default.GetInstance<IBlockTexturesService>();

            //Act
            blocktextures.LoadTextures();

            //Assert
            Assert.IsTrue(blocktextures.IsTexturesLoaded);
        }

        [TestMethod]
        public void BlockTexture_EmptyBlockTexture_Loaded()
        {
            //Arrange
            IBlockTexturesService blocktextures = SimpleIoc.Default.GetInstance<IBlockTexturesService>();

            //Act
            blocktextures.LoadTextures();

            //Assert
            Assert.IsNotNull(blocktextures.EmptyBlockTexture);
        }

        [TestMethod]
        public void BlockTexture_DarkBlueBlockTexture_Loaded()
        {
            //Arrange
            IBlockTexturesService blocktextures = SimpleIoc.Default.GetInstance<IBlockTexturesService>();

            //Act
            blocktextures.LoadTextures();

            //Assert
            Assert.IsNotNull(blocktextures.DarkBlueBlockTexture);
        }

        [TestMethod]
        public void BlockTexture_YellowBlockTexture_Loaded()
        {
            //Arrange
            IBlockTexturesService blocktextures = SimpleIoc.Default.GetInstance<IBlockTexturesService>();

            //Act
            blocktextures.LoadTextures();

            //Assert
            Assert.IsNotNull(blocktextures.YellowBlockTexture);
        }

        [TestMethod]
        public void BlockTexture_BlueBlockTexture_Loaded()
        {
            //Arrange
            IBlockTexturesService blocktextures = SimpleIoc.Default.GetInstance<IBlockTexturesService>();

            //Act
            blocktextures.LoadTextures();

            //Assert
            Assert.IsNotNull(blocktextures.BlueBlockTexture);
        }

        [TestMethod]
        public void BlockTexture_RedBlockTexture_Loaded()
        {
            //Arrange
            IBlockTexturesService blocktextures = SimpleIoc.Default.GetInstance<IBlockTexturesService>();

            //Act
            blocktextures.LoadTextures();

            //Assert
            Assert.IsNotNull(blocktextures.RedBlockTexture);
        }

        [TestMethod]
        public void BlockTexture_GreenBlockTexture_Loaded()
        {
            //Arrange
            IBlockTexturesService blocktextures = SimpleIoc.Default.GetInstance<IBlockTexturesService>();

            //Act
            blocktextures.LoadTextures();

            //Assert
            Assert.IsNotNull(blocktextures.GreenBlockTexture);
        }

        [TestMethod]
        public void BlockTexture_PurpleBlockTexture_Loaded()
        {
            //Arrange
            IBlockTexturesService blocktextures = SimpleIoc.Default.GetInstance<IBlockTexturesService>();

            //Act
            blocktextures.LoadTextures();

            //Assert
            Assert.IsNotNull(blocktextures.PurpleBlockTexture);
        }

        [TestMethod]
        public void BlockTexture_OrangeBlockTexture_Loaded()
        {
            //Arrange
            IBlockTexturesService blocktextures = SimpleIoc.Default.GetInstance<IBlockTexturesService>();

            //Act
            blocktextures.LoadTextures();

            //Assert
            Assert.IsNotNull(blocktextures.OrangeBlockTexture);
        }
    }
}
