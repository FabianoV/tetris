﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.ViewModel;

namespace Tetris.UnitTest
{
    [TestClass]
    public class BaseTests
    {
        public AutoFacContainer Vml { get; set; }

        [TestInitialize]
        public void Initialize()
        {
            this.Vml = new AutoFacContainer();
        }
    }
}
